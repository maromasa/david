package ext.david.ui.vaadin;

import ext.david.model.core.User;
import ext.david.model.core.account.UserAccount;
import ext.david.ui.vaadin.core.SideBar;

import java.util.Set;

public interface UILoginHandler {

    void updateAccessGrants(Set<SideBar.AbstractMenuItem> menuItems);

    UserAccount getCurrentUser();

    boolean isAuthenticated();

    boolean attemptLogin(String username, String password);

    boolean attemptLogout();

    interface LoginSuccessHandler {
        void handleLoginSuccess();
    }

    interface LogoutSuccessHandler {
        void handleLogoutSuccess();
    }

}
