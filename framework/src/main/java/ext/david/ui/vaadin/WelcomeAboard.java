package ext.david.ui.vaadin;

import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

public class WelcomeAboard extends VerticalLayout {

    private void init() {
        Label title = new Label("Welcome Aborad!");
        title.addStyleName(ValoTheme.LABEL_H2);
        addComponent(title);
    }
}
