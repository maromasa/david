package ext.david.ui.vaadin.view;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.themes.ValoTheme;
import ext.david.ui.vaadin.AbstractApplicationView;
import ext.david.ui.vaadin.SideBarContent;
import ext.david.ui.vaadin.SideBarGroup;
import org.h2.value.ValueLob;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import java.util.concurrent.ScheduledExecutorService;


@SideBarContent(caption = "Job Manager", icon = VaadinIcons.STETHOSCOPE, group = @SideBarGroup(caption = "Administration"))
public class JobManagerView extends AbstractApplicationView {

    private ThreadPoolTaskScheduler threadPoolTaskScheduler;

    public JobManagerView() {
        super("Job Manager");
        initComponents();
    }

    public ThreadPoolTaskScheduler getThreadPoolTaskScheduler() {
        return threadPoolTaskScheduler;
    }

    public void setThreadPoolTaskScheduler(ThreadPoolTaskScheduler threadPoolTaskScheduler) {
        this.threadPoolTaskScheduler = threadPoolTaskScheduler;
    }


    private void initComponents() {
        Button schedulerEnableButton = new Button("Disable Scheduler");
        schedulerEnableButton.addStyleName(ValoTheme.BUTTON_DANGER);
        schedulerEnableButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                threadPoolTaskScheduler.getScheduledExecutor().shutdown();
            }
        });
        Grid grid = new Grid<>();
        addComponent(new HorizontalLayout(schedulerEnableButton));
        addComponent(grid);
    }

}
