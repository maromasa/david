package ext.david.ui.vaadin;

import com.vaadin.data.Binder;
import com.vaadin.data.provider.DataChangeEvent;
import com.vaadin.data.provider.DataProviderListener;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.event.selection.SelectionEvent;
import com.vaadin.event.selection.SelectionListener;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import ext.david.ui.vaadin.view.UserManagerView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;

/**
 * @param <T> Entity Type
 */
public abstract class AbstractCrudView<T> extends AbstractApplicationView implements Button.ClickListener, SelectionListener<T> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractCrudView.class);
    protected ListDataProvider<T> listDataProvider;
    protected Grid<T> grid;
    protected Window modalWindow;
    private boolean enableMiniDetails;
    private boolean enableActionButtons;
    private Button createNewButton;
    private Button printButton;
    private VerticalLayout gridViewContainer;
    private HorizontalLayout gridWithQuickDetailContainer;
    private AbstractEntityDetail previewEntityDetail;

    public AbstractCrudView(String caption) {
        super(caption);
        init();
    }

    public boolean isEnableActionButtons() {
        return enableActionButtons;
    }

    public void setEnableActionButtons(boolean enableActionButtons) {
        this.enableActionButtons = enableActionButtons;
    }

    public boolean isEnableMiniDetails() {
        return enableMiniDetails;
    }

    public void setEnableMiniDetails(boolean enableMiniDetails) {
        this.enableMiniDetails = enableMiniDetails;
        if (enableMiniDetails) createQuickDetailView();
    }

    @Override
    public void selectionChange(SelectionEvent<T> event) {
        LOGGER.trace("selectionChange: event={}", event);
        onGridItemSelected(event);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        LOGGER.trace("#enter:");

        previewEntityDetail = createEntityDetail(FormMode.PREVIEW_FROM);

        configureGrid(grid);

//        // append action columns on grid
//        Grid.Column<T, String> column = grid.addColumn(t -> "Edit").setCaption("Actions");
//        column.setRenderer(new ButtonRenderer<>(rendererClickEvent -> {
//            LOGGER.info("Edit Button Clicked");
//            showEditDetailView();
//        }));
//
//        createEntityFormForDetail();
//        addComponent(editDetaillView);

        // bind data for initial.
        listDataProvider = new ListDataProvider<>(getInitialData());
        listDataProvider.addDataProviderListener(new DataProviderListener<T>() {
            @Override
            public void onDataChange(DataChangeEvent<T> event) {
                LOGGER.trace("#onDataChagne: event={}, items={}", event, listDataProvider.getItems());
            }
        });
        grid.setDataProvider(listDataProvider);

        // Continue to event chain to sub-class..
        super.enter(viewChangeEvent);
    }

    @Override
    public void buttonClick(Button.ClickEvent event) {
        Button b = event.getButton();
        if (b == createNewButton) {
            onCreateNewButtonClicked(event);
        }
    }

    public Grid<T> getGrid() {
        return this.grid;
    }

    protected void onGridItemSelected(SelectionEvent<T> event) {
        if (event.getAllSelectedItems().size() == 1 && isPreviewEntityDetailsEnabled()) {
            event.getFirstSelectedItem().ifPresent(t -> previewEntityDetail.setEntity(t));
        }
    }

    protected abstract AbstractEntityDetail createEntityDetail(FormMode formMode);


//    protected AbstractEntityDetail createEntityDetail(FormMode formMode) {
//        Class<? extends AbstractEntityDetail> detailClass = getEntityDetailClass();
//        try {
//            Constructor<? extends AbstractEntityDetail> constructor = detailClass.getDeclaredConstructor(FormMode.class);
//            AbstractEntityDetail instance = constructor.newInstance(formMode);
//            return instance;
//        } catch (InstantiationException e) {
//            e.printStackTrace();
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        } catch (InvocationTargetException e) {
//            e.printStackTrace();
//        } catch (NoSuchMethodException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }

    @Override
    protected AbstractOrderedLayout getContainerComponent() {
        return new VerticalLayout();
    }

    protected void viewChanged(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        LOGGER.debug("#viewChanged: {}", viewChangeEvent);
    }

    protected abstract void configureGrid(Grid<T> grid);

    protected abstract Class<T> getEntityClass();

    protected abstract Collection<T> getInitialData();

    protected abstract T getNewData();

    protected abstract Class<? extends AbstractEntityDetail> getEntityDetailClass();

    protected void onCreateNewButtonClicked(Button.ClickEvent event) {
        createModalWindow();
        AbstractEntityDetail entityDetails = this.createEntityDetail(FormMode.CREATE_FORM);
        entityDetails.getCancelButton().addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                modalWindow.close();
            }
        });
        modalWindow.center();
        modalWindow.setContent(entityDetails);
        if (entityDetails.getEntity() == null) {
            entityDetails.setEntity(getNewData());
        }
        getUI().addWindow(modalWindow);
    }

    protected void onCreateSucceeded(T newData) {
        LOGGER.trace("#onCreateSucceeded: newData={}", newData);
        listDataProvider.getItems().add(newData);
        listDataProvider.refreshAll();
        modalWindow.close();
    }

    protected void onUpdateSucceeded(T updateData) {
        LOGGER.trace("#onUpdateSucceeded: newData={}", updateData);
    }

    protected void onGridItemDoubleClicked(Grid.ItemClick<T> event) {
    }

    private boolean isPreviewEntityDetailsEnabled() {
        return (previewEntityDetail != null);
    }

    private void createModalWindow() {
        if (this.modalWindow != null) {
            return;
        }
        modalWindow = new Window("Add New");
        modalWindow.setClosable(true);
        modalWindow.setModal(true);
        modalWindow.setSizeUndefined();
    }

    private void init() {
        enableMiniDetails = false;

        // tool buttons above grid.
        createNewButton = new Button("Create New");
        createNewButton.addClickListener(this);
        createNewButton.addStyleName(ValoTheme.BUTTON_SMALL);
        createNewButton.addStyleName(ValoTheme.BUTTON_FRIENDLY);

        printButton = new Button("print");
        printButton.addStyleName(ValoTheme.BUTTON_SMALL);
        printButton.addClickListener(this);

        // grid.
        grid = new Grid<>(getEntityClass());
        grid.setSizeFull();

        // row selection event on grid
        grid.addSelectionListener(this);
//        grid.addSelectionListener(new SelectionListener<T>() {
//            @Override
//            public void selectionChange(SelectionEvent<T> selectionEvent) {
//                LOGGER.info("grid row selected: {}", selectionEvent);
//                if (enableMiniDetails) {
//                    Optional<T> bean = selectionEvent.getFirstSelectedItem();
//                    if (bean.isPresent()) {
//                        quickEntityForm.binder.setBean(bean.get());
//                        if (quickEntityForm.isVisible() == false) {
//                            quickEntityForm.setVisible(true);
//                        }
//                    }
//                }
//            }
//        });

//        grid.addItemClickListener(this);

        gridWithQuickDetailContainer = new HorizontalLayout();
        gridWithQuickDetailContainer.addComponents(grid);
        gridWithQuickDetailContainer.setSizeFull();

        gridViewContainer = new VerticalLayout();
        gridViewContainer.addComponents(new HorizontalLayout(createNewButton, printButton), gridWithQuickDetailContainer);
        gridViewContainer.setExpandRatio(gridWithQuickDetailContainer, 1);
        gridViewContainer.setSizeFull();
        gridViewContainer.setMargin(false);

        addComponent(gridViewContainer);
        setExpandRatio(gridViewContainer, 1);
    }

    private void createQuickDetailView() {
    }


    protected enum FormMode {
        CREATE_FORM, MODIFY_FORM, DETAIL_FORM, PREVIEW_FROM
    }

    public abstract class AbstractEntityDetail extends VerticalLayout implements Button.ClickListener {

        private final FormMode formMode;
        protected Button submitButton;
        protected Button cancelButton;
        protected ComponentContainer formControls;
        protected HorizontalLayout bottomControls;
        protected Binder<T> binder;

        public AbstractEntityDetail(FormMode formMode) {
            LOGGER.trace("#CONSTRUCTOR: formMode={}, type={}", formMode, getClass());
            this.formMode = formMode;
            initUIControls();
            configUIControlsByMode();
            binder = new Binder(getEntityClass());
        }

        public HorizontalLayout getBottomControls() {
            return bottomControls;
        }

        public FormMode getFormMode() {
            return this.formMode;
        }

        @Override
        public void attach() {
            super.attach();
            switch (formMode) {
                case DETAIL_FORM:
                    initForDetail(formControls, binder);
                    break;
                case CREATE_FORM:
                    initForCreate(formControls, binder);
                    break;
                case MODIFY_FORM:
                    initForModify(formControls, binder);
                    break;
                case PREVIEW_FROM:
                    initForPreview(formControls, binder);
                    break;
            }
            LOGGER.trace("#attach: mode={}, caption={}, instance={}", formMode, getCaption(), this.toString());
        }

        @Override
        public void buttonClick(Button.ClickEvent event) {
            LOGGER.trace("#buttonClick: object={}; event={}", this.toString(), event);
            Button b = event.getButton();
            if (b == this.submitButton) {
                onSubmitButtonClicked();
            } else if (b == this.cancelButton) {
                onCancelButtonClicked();
            }
        }

        public Button getSubmitButton() {
            return submitButton;
        }

        public Button getCancelButton() {
            return cancelButton;
        }

        protected void onSubmitButtonClicked() {
            // TODO:: Confirmation Dialog before perform.
            LOGGER.trace("#onSubmitButtonClicked:");
            switch (formMode) {
                case CREATE_FORM:
                    onSubmitButtonClickedForCreate();
                    break;
                case MODIFY_FORM:
                    onSubmitButtonClickedForUpdate();
                    break;
            }
        }

        protected void onSubmitButtonClickedForCreate() {
            if (binder.isValid()) {
                AbstractCrudView.this.onCreateSucceeded(getEntity());
            }
        }

        protected void onSubmitButtonClickedForUpdate() {
            if (binder.isValid()) {
                AbstractCrudView.this.onUpdateSucceeded(getEntity());
            }
        }

        protected T getEntity() {
            return binder.getBean();
        }

        protected void setEntity(T entity) {
            binder.setBean(entity);
        }

        protected void onCancelButtonClicked() {
        }

        protected ComponentContainer createFormControlsContainer(FormMode formMode) {
            if (formMode.equals(UserManagerView.FormMode.PREVIEW_FROM)) {
                return new CssLayout();
            } else {
                return new FormLayout();
            }
        }

        protected abstract void initForCreate(ComponentContainer formControls, Binder<T> binder);

        protected abstract void initForModify(ComponentContainer formControls, Binder<T> binder);

        protected abstract void initForPreview(ComponentContainer formControls, Binder<T> binder);

        protected abstract void initForDetail(ComponentContainer formControls, Binder<T> binder);

        protected void configUIControlsByMode() {
            switch (formMode) {
                case CREATE_FORM:
                    submitButton.setCaption("Create");
                    submitButton.addStyleName(ValoTheme.BUTTON_FRIENDLY);
                    break;
                case MODIFY_FORM:
                    submitButton.setCaption("Update");
                    submitButton.addStyleName(ValoTheme.BUTTON_PRIMARY);
                    break;
                case DETAIL_FORM:
                    submitButton.setVisible(false);
                    break;
                case PREVIEW_FROM:
                    submitButton.setVisible(false);
                    break;
            }
        }

        private void initUIControls() {
            submitButton = new Button("Submit");
            cancelButton = new Button("Cancel");

            bottomControls = new HorizontalLayout();
            Label spacer = new Label();
            bottomControls.setWidth(100, Unit.PERCENTAGE);
            bottomControls.addComponents(spacer, submitButton, cancelButton);
            bottomControls.setExpandRatio(spacer, 1);

            formControls = createFormControlsContainer(formMode);
            formControls.setSizeUndefined();

            addComponents(formControls, bottomControls);
            setExpandRatio(formControls, 1);
            setSizeUndefined();

            submitButton.addClickListener(this);
            cancelButton.addClickListener(this);

        }
    }

}
