package ext.david.ui.vaadin.core;

import com.vaadin.data.provider.DataChangeEvent;
import com.vaadin.data.provider.DataProviderListener;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.Resource;
import com.vaadin.server.ThemeResource;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import com.vaadin.util.ReflectTools;
import ext.david.model.core.User;
import ext.david.model.core.account.UserAccount;
import javafx.geometry.Side;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.Method;
import java.util.*;

@SpringComponent
@UIScope
public class SideBar extends VerticalLayout {

    private static final Logger LOGGER = LoggerFactory.getLogger(SideBar.class);

    private DefaultVaadinApplicationUI parentUI;
    private boolean enableHomeMenu = true;
    private boolean menuComponentCreated = false;
    private boolean loginFeatureEnabled = false;
    private Set<AbstractMenuItem> menuItems = new HashSet<>();
    private Label applicationNameLabel;
    private ComponentContainer menuItemUIContainer;
    private MenuBar profileMenuBar;
    private MenuBar.MenuItem userMenuItem;
    private MenuBar.MenuItem preferenceMenuItem;
    private MenuBar.MenuItem messageMenuItem;
    private Button logoutButton;
    private ListDataProvider<AbstractMenuItem> menuItemListDataProvider = new ListDataProvider<>(menuItems);
    private MenuBarCommandConsumer menuBarCommandConsumer = new MenuBarCommandConsumer();

    public SideBar() {
        LOGGER.trace("#CONSTRUCTOR: this={}", getClass());
        init();
    }

    public void addLogoutButtonClickListener(Button.ClickListener clickListener) {
        logoutButton.addClickListener(clickListener);
    }

    public void addMenuItemClickListener(MenuItemClickListener listener) {
        addListener(MenuItemClickEvent.class, listener, MenuItemClickListener.EVENT_METHOD_NAME);
    }

    public void buildMenuUIComponet(boolean force) {
        drawMenuUIComponents(force);
    }

    public String getTitleCaption() {
        return applicationNameLabel.getValue();
    }

    public void setTitleCaption(String caption) {
        applicationNameLabel.setValue(caption);
    }

    public boolean isEnableHomeMenu() {
        return enableHomeMenu;
    }

    public void setEnableHomeMenu(boolean enableHomeMenu) {
        this.enableHomeMenu = enableHomeMenu;
    }

    public boolean isLoginFeatureEnabled() {
        return loginFeatureEnabled;
    }

    public void setLoginFeatureEnabled(boolean loginFeatureEnabled) {
        profileMenuBar.setVisible(loginFeatureEnabled);
        logoutButton.setVisible(loginFeatureEnabled);
        this.loginFeatureEnabled = loginFeatureEnabled;
    }

    public void setMessageMenuItemCommand(MenuBar.Command command) {
        messageMenuItem.setCommand(command);
    }

    public void setParentUI(DefaultVaadinApplicationUI parentUI) {
        LOGGER.trace("#setParentUI: ui={}", parentUI);
        this.parentUI = parentUI;
        parentUI.addUserChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.trace("#propertyChange: event={}", evt);
                UserAccount u = (UserAccount) evt.getNewValue();
                userMenuItem.setText(u.getUsername());
            }
        });
    }

    public Button getLogoutButton() {
        return logoutButton;
    }

    public void setPreferenceMenuItemCommand(MenuBar.Command command) {
        preferenceMenuItem.setCommand(command);
    }

    protected Set<AbstractMenuItem> getMenuItems() {
        return menuItems;
    }

    protected void setMenuItems(Set<AbstractMenuItem> menuItems) {
        this.menuItems = menuItems;
    }

    protected void drawMenuUIComponents(boolean force) {
        LOGGER.trace("#drawMenuUIComponents: force='{}'", force);

        if (force || menuComponentCreated == false) {

            LOGGER.debug("Now creates MenuItem-UI-Object as Labels or Buttons, from the collection: {}", menuItems);
            menuItemUIContainer.removeAllComponents();

            if (enableHomeMenu) {
                MenuItem homeMenu = new MenuItem("HOME", "", VaadinIcons.HOME, Integer.MIN_VALUE);
                menuItemUIContainer.addComponent(homeMenu.getUIComponent(this));
            }

            if (!menuItems.isEmpty()) {
                Set<AbstractMenuItem> sorted = sortMenuItems(this.menuItems);
                for (AbstractMenuItem item : sorted) {
                    appendMenuUIComponent(item, menuItemUIContainer);
                }
            }
            menuComponentCreated = true;
        }

    }

    protected void menuItemClicked(Button component, MenuItem item) {
        LOGGER.trace("#menuItemClicked: source=[{}], data=[{}]", component, item);
        MenuItemClickEvent event = new MenuItemClickEvent(component, item);
        fireEvent(event);
    }

    private void onUserMenuItemSelected() {
        LOGGER.trace("#onUserMenuItemSelected");
        parentUI.openPreference();
    }

    private void appendMenuUIComponent(AbstractMenuItem item, ComponentContainer container) {
        LOGGER.trace("#appendMenuUIComponent: item={}", item);

        if (item.isAccessible()) {
            container.addComponent(item.getUIComponent(this));
        } else {
            LOGGER.debug("Excluded for appending SideBar UI controls because of access permission: {}", item.caption);
        }

        // if item is group, traverse into children.
        if (item.hasChildren()) {
            for (AbstractMenuItem child : item.children) {
                appendMenuUIComponent(child, container);
            }
        }
    }

    private ComponentContainer createMenuItemContainer() {
        ComponentContainer container = new CssLayout();
        container.addStyleName("valo-menuitems");
        container.setHeight(100, Unit.PERCENTAGE);
        container.setWidthUndefined();
        return container;
    }

    private void init() {
        LOGGER.trace("#init");

        // application title of side bar(top)
        applicationNameLabel = new Label();
        HorizontalLayout titleContainer = new HorizontalLayout();
        titleContainer.addStyleName(ValoTheme.MENU_TITLE);
        titleContainer.addComponents(applicationNameLabel);

        // user profile area
        profileMenuBar = new MenuBar();
        profileMenuBar.addStyleName("user-menu");
        userMenuItem = profileMenuBar.addItem("john doe", new ThemeResource("img/default-profile-icon.png"), menuBarCommandConsumer);
//        preferenceMenuItem = userMenuItem.addItem("", VaadinIcons.CONTROLLER, null);
//        messageMenuItem = userMenuItem.addItem("", VaadinIcons.ENVELOPE, null);
//        userMenuItem.addSeparator();
//        MenuBar.MenuItem logoutMenuItem = userMenuItem.addItem("Logout", VaadinIcons.EXIT, null);

        // container for menu list area
        menuItemUIContainer = createMenuItemContainer();

        // logout button
        logoutButton = new Button("LOGOUT");
        logoutButton.addStyleName(ValoTheme.BUTTON_DANGER);
        logoutButton.addStyleName(ValoTheme.BUTTON_SMALL);
        logoutButton.setHeight(80, Unit.PERCENTAGE);
        HorizontalLayout logoutButtonContainer = new HorizontalLayout(logoutButton);
        logoutButtonContainer.setComponentAlignment(logoutButton, Alignment.MIDDLE_CENTER);

        // the layout layer for root container
        VerticalLayout rootLayout = new VerticalLayout();
        rootLayout.addComponents(titleContainer, profileMenuBar, menuItemUIContainer, logoutButtonContainer);
        rootLayout.setExpandRatio(menuItemUIContainer, 1);
        rootLayout.setComponentAlignment(logoutButtonContainer, Alignment.MIDDLE_CENTER);
        rootLayout.addStyleName(ValoTheme.MENU_PART);
        rootLayout.setSpacing(false);
        rootLayout.setMargin(false);
        rootLayout.setSizeFull();

        // the root container of this
        CssLayout root = new CssLayout();
        root.setStyleName(ValoTheme.MENU_ROOT);
        root.setSizeFull();
        root.addComponents(rootLayout);

        // this
        super.addComponent(root);
        setSpacing(false);
        setMargin(false);
        setHeight(100, Unit.PERCENTAGE);
        setWidthUndefined();

        // Event Listeners
        menuItemListDataProvider.addDataProviderListener(new DataProviderListener<AbstractMenuItem>() {
            @Override
            public void onDataChange(DataChangeEvent<AbstractMenuItem> dataChangeEvent) {
                LOGGER.info("#onDataChange: {}", dataChangeEvent);
            }
        });
    }

    private void menuBarMenuItemClicked(MenuBar.MenuItem menuItem) {
    }

    private Set<AbstractMenuItem> sortMenuItemRecurse(Set<AbstractMenuItem> items, Comparator comparator) {
        Set<AbstractMenuItem> sorted = new TreeSet<>(comparator);
        for (AbstractMenuItem item : items) {
            if (item.hasChildren()) {
                item.children = sortMenuItemRecurse(item.children, comparator);
            }
        }
        sorted.addAll(items);
        return sorted;
    }

    private Set<AbstractMenuItem> sortMenuItems(Set<AbstractMenuItem> target) {
        Set<AbstractMenuItem> sorted = sortMenuItemRecurse(target, new MenuItemComparator());
        return sorted;
    }

    public interface MenuItemClickListener extends EventListener {
        Method EVENT_METHOD_NAME = ReflectTools.findMethod(MenuItemClickListener.class, "menuItemClick", MenuItemClickEvent.class);

        void menuItemClick(MenuItemClickEvent event);
    }

    public static class MenuItem extends AbstractMenuItem implements Button.ClickListener {

        public MenuItem(String caption, String viewName, Resource icon, int weight) {
            super(caption, viewName, icon, weight);
        }

        @Override
        public void buttonClick(Button.ClickEvent event) {
            parent.menuItemClicked(event.getButton(), this);
        }

        @Override
        protected AbstractComponent createUIComponent() {
            Button uiComponent = new Button(caption, icon);
            uiComponent.setPrimaryStyleName(ValoTheme.MENU_ITEM);
            uiComponent.addClickListener(this);
            return uiComponent;
        }

    }

    public static abstract class AbstractMenuItem {
        String caption;
        String viewName;
        Resource icon;
        int weight;
        SideBar parent;
        AbstractComponent uiComponent;
        String[] accessRoles = {};
        boolean accessible = true;
        Set<AbstractMenuItem> children = new HashSet<>();

        AbstractMenuItem(String caption, String viewName, Resource icon, int weight) {
            this.caption = caption;
            this.viewName = viewName;
            this.icon = icon;
            this.weight = weight;
        }

        public boolean isAccessible() {
            return accessible;
        }

        public void setAccessible(boolean accessible) {
            this.accessible = accessible;
        }

        public String[] getAccessRoles() {
            return accessRoles;
        }

        public void setAccessRoles(String... accessRoles) {
            this.accessRoles = accessRoles;
        }

        public boolean hasChildren() {
            return (children.size() > 0);
        }

        public Set<AbstractMenuItem> getChildren() {
            return this.children;
        }

        public boolean addAll(Collection<AbstractMenuItem> c) {
            return (this instanceof MenuGroup) ? children.addAll(c) : false;
        }

        public boolean addChild(AbstractMenuItem abstractMenuItem) {
            return (this instanceof MenuGroup) ? children.add(abstractMenuItem) : false;
        }

        public void clear() {
            children.clear();
        }

        public String getCaption() {
            return caption;
        }

        public void setCaption(String caption) {
            this.caption = caption;
        }

        public Resource getIcon() {
            return icon;
        }

        public void setIcon(Resource icon) {
            this.icon = icon;
        }

        public String getViewName() {
            return viewName;
        }

        public void setViewName(String viewName) {
            this.viewName = viewName;
        }

        public int getWeight() {
            return weight;
        }

        public void setWeight(int weight) {
            this.weight = weight;
        }

        public boolean isMenuGroup() {
            return (this instanceof MenuGroup);
        }

        public boolean remove(AbstractMenuItem o) {
            return children.remove(o);
        }

        public boolean removeAll(Collection<AbstractMenuItem> c) {
            return children.removeAll(c);
        }

        Component getUIComponent(SideBar parent) {
            setSideBar(parent);
            if (uiComponent == null) {
                uiComponent = createUIComponent();
            }
            return uiComponent;
        }

        void setSideBar(SideBar sideBar) {
            this.parent = sideBar;
        }

        protected abstract AbstractComponent createUIComponent();

    }

    public static class MenuItemClickEvent extends Component.Event {

        private MenuItem menuItem;

        public MenuItemClickEvent(Component source, MenuItem menuItem) {
            super(source);
            this.menuItem = menuItem;
        }

        public String getCaption() {
            return menuItem.caption;
        }

        public String getViewName() {
            return menuItem.viewName;
        }

    }

    public static class MenuGroup extends AbstractMenuItem {

        public MenuGroup(String caption, Resource icon, int weight) {
            super(caption, null, icon, weight);
        }

        @Override
        protected AbstractComponent createUIComponent() {
            Label component = new Label(caption);
            component.addStyleName(ValoTheme.MENU_SUBTITLE);
            return component;
        }
    }

    private static class MenuItemComparator implements Comparator<AbstractMenuItem> {

        @Override
        public int compare(AbstractMenuItem o1, AbstractMenuItem o2) {
            if (o1.getClass().equals(o2.getClass())) {
                LOGGER.trace("#compare: o1={}, o2={}, result={}", o1, o2, o2.weight - o1.weight);
                return (o2.weight != o1.weight) ? o1.weight - o2.weight : o1.caption.compareTo(o2.caption);
            } else {
                return (o1 instanceof MenuGroup) ? 1 : -1;
            }
        }
    }

    private class MenuBarCommandConsumer implements MenuBar.Command {

        @Override
        public void menuSelected(MenuBar.MenuItem selectedItem) {
            if (selectedItem == userMenuItem) {
                SideBar.this.onUserMenuItemSelected();
            }
        }
    }

}
