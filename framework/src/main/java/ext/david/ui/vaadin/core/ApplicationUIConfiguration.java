package ext.david.ui.vaadin.core;

import org.springframework.beans.factory.annotation.Value;

public class ApplicationUIConfiguration {

    @Value("${david.application.name}")
    private String applicationName;

    @Value("${david.application.loginDescription:}")
    private String loginDescription;

    private boolean securityFeatureEnabled;

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getLoginDescription() {
        return loginDescription;
    }

    public void setLoginDescription(String loginDescription) {
        this.loginDescription = loginDescription;
    }

    public boolean isSecurityFeatureEnabled() {
        return securityFeatureEnabled;
    }

    public void setSecurityFeatureEnabled(boolean securityFeatureEnabled) {
        this.securityFeatureEnabled = securityFeatureEnabled;
    }
}
