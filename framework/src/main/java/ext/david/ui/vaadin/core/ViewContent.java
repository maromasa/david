package ext.david.ui.vaadin.core;

import com.vaadin.spring.annotation.SpringViewDisplay;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.CssLayout;

@SpringViewDisplay
@UIScope
public class ViewContent extends CssLayout {

    public ViewContent() {
        init();
    }

    private void init() {
        setSizeFull();
    }
}
