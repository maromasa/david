package ext.david.ui.vaadin.view;

import com.vaadin.icons.VaadinIcons;
import ext.david.ui.vaadin.AbstractApplicationView;
import ext.david.ui.vaadin.SideBarContent;
import ext.david.ui.vaadin.SideBarGroup;

@SideBarContent(caption = "System Settings", icon = VaadinIcons.CONTROLLER, weight = 900, group = @SideBarGroup(caption = "Administration"))
public class SystemSettingsView extends AbstractApplicationView {

    public SystemSettingsView() {
        super("System Settings");
    }
}
