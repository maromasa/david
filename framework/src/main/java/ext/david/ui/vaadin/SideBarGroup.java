package ext.david.ui.vaadin;

import com.vaadin.icons.VaadinIcons;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface SideBarGroup {

    String caption() default "";

    VaadinIcons icon() default VaadinIcons.TOOLBOX;

    int weight() default 100;
}
