package ext.david.ui.vaadin.view;

import com.vaadin.data.Binder;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import ext.david.model.core.User;
import ext.david.ui.vaadin.AbstractCrudView;
import ext.david.ui.vaadin.SideBarContent;
import ext.david.ui.vaadin.SideBarGroup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;

@SideBarContent(caption = "User Manager", icon = VaadinIcons.USERS, group = @SideBarGroup(caption = "Administration"), hasRoles = {"ROLE_MANAGER"})
public class UserManagerView extends AbstractCrudView<User> {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserManagerView.class);

    private ArrayList<User> users = new ArrayList<>();

    public UserManagerView() {
        super("User Manager");
    }


    protected void onCreateSucceeded(UserApplicant userApplicant) {
        LOGGER.trace("#onCreateSucceeded: applicant={}", userApplicant);
        User u = User.withUser(userApplicant.getUsername()).password(userApplicant.password1).email(userApplicant.email).roles("ROLE_USER").build();
        users.add(u);
        listDataProvider.refreshAll();
        modalWindow.close();
    }

    @Override
    protected AbstractEntityDetail createEntityDetail(FormMode formMode) {
        return new UserManagerDetail(formMode);
    }

    @Override
    protected void configureGrid(Grid<User> grid) {
    }

    @Override
    protected Class<User> getEntityClass() {
        return User.class;
    }

    @Override
    protected Collection<User> getInitialData() {
        LOGGER.trace("#getInitialData");
//        users.addConsumer(getParent().getCurrentUser());
        return users;
    }

    @Override
    protected User getNewData() {
        return null;
    }

    @Override
    protected Class<UserManagerDetail> getEntityDetailClass() {
        return UserManagerDetail.class;
    }

    /**
     * User model for applicant, because of Immutable.
     */
    public static class UserApplicant {
        String username;
        String password1;
        String password2;
        String email;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword1() {
            return password1;
        }

        public void setPassword1(String password1) {
            this.password1 = password1;
        }

        public String getPassword2() {
            return password2;
        }

        public void setPassword2(String password2) {
            this.password2 = password2;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }

    public class UserManagerDetail extends AbstractEntityDetail {

        Binder<UserApplicant> userApplicantBinder = new Binder<>(UserApplicant.class);

        private TextField username = new TextField("username");
        private TextField emailAddress = new TextField("email");
        private TextField password = new TextField("password");
        private TextField password2 = new TextField("password2");

        public UserManagerDetail(FormMode formMode) {
            super(formMode);
        }

        @Override
        protected void initForCreate(ComponentContainer formControls, Binder<User> binder) {
            formControls.addComponents(username, password, password2, emailAddress);
            userApplicantBinder.forField(username).bind("username");
            userApplicantBinder.forField(password).bind("password1");
            userApplicantBinder.forField(password2).bind("password2");
            userApplicantBinder.forField(emailAddress).bind("email");
            userApplicantBinder.setBean(new UserApplicant());
        }

        @Override
        protected void onSubmitButtonClickedForCreate() {
            if (userApplicantBinder.isValid()) {
                UserManagerView.this.onCreateSucceeded(userApplicantBinder.getBean());
            }
        }

        @Override
        protected void initForModify(ComponentContainer formControls, Binder<User> binder) {
            username.setReadOnly(true);
            formControls.addComponents(username, password2, emailAddress);
            binder.forField(username).bind("username");
            binder.forField(emailAddress).bind("email");
        }

        @Override
        protected void initForPreview(ComponentContainer formControls, Binder<User> binder) {
            formControls.addComponents(username, password2, emailAddress);
            binder.forField(username).bind("username");
        }

        @Override
        protected void initForDetail(ComponentContainer formControls, Binder<User> binder) {
            Label spacer = new Label();
            Button modify = new Button("Modify");
            Button delete = new Button("Delete");
            modify.addStyleName(ValoTheme.BUTTON_PRIMARY);
            delete.addStyleName(ValoTheme.BUTTON_DANGER);
            getBottomControls().addComponents(spacer, modify, delete);
            getBottomControls().setExpandRatio(spacer, 1);

            formControls.addComponents(username, emailAddress, password2);
            username.setReadOnly(true);
            emailAddress.setReadOnly(true);
            password2.setReadOnly(true);
            binder.forField(username).bind("username");
            binder.forField(emailAddress).bind("email");
            binder.forField(password2).bind("password2");

        }

    }

}
