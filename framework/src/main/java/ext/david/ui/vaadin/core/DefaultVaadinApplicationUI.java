package ext.david.ui.vaadin.core;

import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.*;
import com.vaadin.shared.ui.window.WindowMode;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import ext.david.model.core.account.UserAccount;
import ext.david.ui.vaadin.SideBarMenuProvider;
import ext.david.ui.vaadin.SimpleLogin;
import ext.david.ui.vaadin.UILoginHandler;
import ext.david.ui.vaadin.UserPreference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.*;

@SpringUI
@Theme("david")
public class DefaultVaadinApplicationUI extends UI {

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultVaadinApplicationUI.class);

    private String applicationName;
    private ApplicationUIConfiguration applicationUIConfiguration;
    private HorizontalLayout root;
    private SideBar sidebar;
    private ViewContent viewContent;
    private SimpleLogin simpleLogin;
    private UILoginHandler uiLoginHandler;
    private List<SideBarMenuProvider> sideBarMenuProviderList = new ArrayList<>();
    private boolean authenticated = false;
    private boolean loginFeatureEnabled = false;
    private UserAccount currentUserAccount;
    private PropertyChangeSupport userPropertyChangeSupport = new PropertyChangeSupport(this);
    private Window preferenceWindow;
    private ResourceBundle resourceBundle;
    private MessageSource messageSource;

    public DefaultVaadinApplicationUI() {
        LOGGER.trace("#CONSTRUCTOR");
    }

    public MessageSource getMessageSource() {
        return messageSource;
    }

    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public ApplicationUIConfiguration getApplicationUIConfiguration() {
        return applicationUIConfiguration;
    }

    @Autowired(required = false)
    public void setApplicationUIConfiguration(ApplicationUIConfiguration applicationUIConfiguration) {
        LOGGER.trace("#setApplicationUIConfiguration: applicationUIConfiguration={}", applicationUIConfiguration);
        this.applicationUIConfiguration = applicationUIConfiguration;
    }

    public UserAccount getCurrentUserAccount() {
        return currentUserAccount;
    }

    public void setCurrentUserAccount(UserAccount currentUserAccount) {
        LOGGER.trace("#setCurrentUserAccount: currentUserAccount={}", currentUserAccount);
        if (this.currentUserAccount == null || !this.currentUserAccount.equals(currentUserAccount)) {
            UserAccount old = this.currentUserAccount;
            this.currentUserAccount = currentUserAccount;
            uiLoginHandler.updateAccessGrants(sidebar.getMenuItems());
            userPropertyChangeSupport.firePropertyChange("currentUserAccount", old, currentUserAccount);
        }
    }

    public void addUserChangeListener(PropertyChangeListener propertyChangeListener) {
        userPropertyChangeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public void openPreference() {
        createPreferenceWindow();
        preferenceWindow.center();
        getUI().addWindow(preferenceWindow);
    }

    public void setUiLoginHandler(UILoginHandler uiLoginHandler) {
        LOGGER.trace("#setLoginHandelr: {}", uiLoginHandler);
        this.uiLoginHandler = uiLoginHandler;
        this.loginFeatureEnabled = true;
    }

    public List<SideBarMenuProvider> getSideBarMenuProviderList() {
        return sideBarMenuProviderList;
    }

    @Autowired(required = false)
    public void setSideBarMenuProviderList(List<SideBarMenuProvider> sideBarMenuProviderList) {
        this.sideBarMenuProviderList = sideBarMenuProviderList;
    }

    public SideBar getSidebar() {
        return sidebar;
    }

    @Autowired
    public void setSidebar(SideBar sidebar) {
        this.sidebar = sidebar;
    }

    public ViewContent getViewContent() {
        return viewContent;
    }

    @Autowired
    public void setViewContent(ViewContent viewContent) {
        this.viewContent = viewContent;
    }

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        LOGGER.trace("#init: vaadinRequest={}", vaadinRequest);
        if (isAuthenticated()) {
            initComponents();
            updateContent();
        } else {
            setNavigator(null);
            getPage().setLocation("/login");
            getSession().close();
        }
    }

    private boolean isAuthenticated() {
        return (uiLoginHandler != null) ? uiLoginHandler.isAuthenticated() : true;
    }

    private void createPreferenceWindow() {
        if (preferenceWindow == null) {
            preferenceWindow = new Window("Preference");
            preferenceWindow.setModal(true);
            preferenceWindow.setWindowMode(WindowMode.MAXIMIZED);
            preferenceWindow.setContent(new UserPreference());
            preferenceWindow.setClosable(true);
        }
    }

    private void attemptLogin(String username, String password) {

        boolean result = uiLoginHandler.attemptLogin(username, password);

        // if login was success
        if (authenticated == false && result == true) {
            authenticated = true;
            setCurrentUserAccount(uiLoginHandler.getCurrentUser());
            updateContent();
            // TODO:: clear TextField values in LoginForm of SimpleLoing when Logged in or Logged out.
        } else {
            // or failed
            // TODO:: update ui information the cause of failed logged in.
        }
    }

    private void initComponents() {
        LOGGER.trace("#initComponents");

        EventConsumer eventConsumer = new EventConsumer();

        if (applicationUIConfiguration != null) {
            applicationName = applicationUIConfiguration.getApplicationName();
        }

        // setting-up default navigator
        if (getUI().getNavigator() != null) {
            getUI().getNavigator().addView("", Navigator.EmptyView.class);
        }

        LOGGER.trace("current locale: {}; defualt={}", VaadinSession.getCurrent().getLocale(), Locale.getDefault());

        VaadinSession.getCurrent().setLocale(Locale.getDefault());

        // setting up Sidebar.
        sidebar.setParentUI(this);
        sidebar.setLoginFeatureEnabled(loginFeatureEnabled);
        sidebar.addMenuItemClickListener(eventConsumer);
        sidebar.setTitleCaption(applicationName);

        Set<SideBar.AbstractMenuItem> menuItems = new HashSet<>();
        for (SideBarMenuProvider provider : sideBarMenuProviderList) {
            menuItems.addAll(provider.provideMenuItems());
        }
        sidebar.setMenuItems(menuItems);

        // if loginFeature enabled, UI should be setup login and multi-user design.
        if (loginFeatureEnabled) {

            // uiLoginHandler Object check.
            if (uiLoginHandler == null) {
                throw new IllegalStateException("uiLoginHandler is null besides loginFeature enabled");
            }

            // create login ui component
            simpleLogin = new SimpleLogin();
            simpleLogin.addLoginListener(eventConsumer);

            simpleLogin.setTitle(messageSource.getMessage("login.title", null, applicationName, Locale.getDefault()));
            simpleLogin.setDescription(messageSource.getMessage("login.description", null, "", Locale.getDefault()));

            // set-up logout button handler in sidebar.
            sidebar.addLogoutButtonClickListener(eventConsumer);

            // update authenticated if already log-in (i.e. rememberMe)
            authenticated = uiLoginHandler.isAuthenticated();
            if (authenticated) {
                setCurrentUserAccount(uiLoginHandler.getCurrentUser());
            }

        }

        // create sidebar MenuItems.
        sidebar.drawMenuUIComponents(true);

        // initialize content root.
        root = new HorizontalLayout();
        root.addComponents(sidebar, viewContent);
        root.setExpandRatio(viewContent, 1);
        root.setSizeFull();

        setSizeFull();

    }

    private void updateContent() {
        LOGGER.trace("#updateContent: rootComponent={}", loginFeatureEnabled ? (authenticated ? root : simpleLogin) : root);
        setContent(loginFeatureEnabled ? (authenticated ? root : simpleLogin) : root);
    }

    private final class EventConsumer implements LoginForm.LoginListener, Button.ClickListener, SideBar.MenuItemClickListener {

        @Override
        public void buttonClick(Button.ClickEvent event) {
            if (event.getButton() == sidebar.getLogoutButton()) {
                uiLoginHandler.attemptLogout();
                // TODO:: consider should be evaluated return value when logout.
                // TODO:: also clear TextField values in LoginForm of Simp  leLogin
                // TODO:: also should be clear UI session no suitable impl that setUriFragment set empty.
                authenticated = false;
                simpleLogin.clearField();
                getPage().setLocation("/");
//                getUI().getPage().setUriFragment("", false);
                updateContent();
            }
        }

        @Override
        public void onLogin(LoginForm.LoginEvent event) {
//            attemptLogin(event.getLoginParameter("username"), event.getLoginParameter("password"));
        }

        @Override
        public void menuItemClick(SideBar.MenuItemClickEvent event) {
            getUI().getNavigator().navigateTo(event.getViewName());
        }
    }

}
