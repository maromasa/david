package ext.david.ui.vaadin;

import ext.david.ui.vaadin.core.SideBar;

import java.util.Set;

public interface SideBarMenuProvider {

    Set<SideBar.AbstractMenuItem> provideMenuItems();

}
