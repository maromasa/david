package ext.david.ui.vaadin;

import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import ext.david.ui.vaadin.core.DefaultVaadinApplicationUI;

public class UserPreference extends HorizontalLayout {

    private DefaultVaadinApplicationUI parentUI;

    public UserPreference() {
        initUIComponents();
    }

    private void initUIComponents() {

        VerticalLayout leftPane = new VerticalLayout();

        VerticalLayout rightPane = new VerticalLayout();


        addComponents(leftPane, rightPane);

    }
}
