package ext.david.ui.vaadin;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@SpringView
public @interface SideBarContent {

    String caption() default "";

    SideBarGroup group() default @SideBarGroup();

    String[] hasRoles() default {};

    VaadinIcons icon() default VaadinIcons.LINES;

    String value() default "";

    @AliasFor(annotation = SpringView.class, attribute = "name")
    String viewName() default SpringView.USE_CONVENTIONS;

    int weight() default 100;

}
