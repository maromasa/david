package ext.david.ui.vaadin;

import com.vaadin.server.VaadinServletService;
import ext.david.model.core.User;
import ext.david.model.core.account.UserAccount;
import ext.david.ui.vaadin.core.SideBar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.access.vote.AffirmativeBased;
import org.springframework.security.access.vote.RoleVoter;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class SpringSecurityUILoginHandler implements UILoginHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(SpringSecurityUILoginHandler.class);

    private final AuthenticationManager authenticationManager;

    private final AccessDecisionManager accessDecisionManager;

    public SpringSecurityUILoginHandler(AuthenticationManager authenticationManager) {
        LOGGER.info("#CONSTRUCTOR: authenticationManager={}", authenticationManager);
        this.authenticationManager = authenticationManager;
        this.accessDecisionManager = new AffirmativeBased(Arrays.asList(new RoleVoter()));
    }

    @Override
    public void updateAccessGrants(Set<SideBar.AbstractMenuItem> menuItems) {
        LOGGER.trace("#updateAccessGrants");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.isAuthenticated()) {
            for (SideBar.AbstractMenuItem item : menuItems) {
                updateAccessGrants(item, authentication);
            }
        }
    }

    @Override
    public UserAccount getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.isAuthenticated() == true) {
            Object details = authentication.getPrincipal();
            if (details instanceof UserAccount) {
                return (UserAccount) details;
            } else {
                LOGGER.warn("Authentication Details is not ApplicationUser instance: {}", details);
                return null;
            }
        } else {
            LOGGER.warn("Not Authenticated");
            return null;
        }
    }

    @Override
    public boolean isAuthenticated() {
        if (SecurityContextHolder.getContext().getAuthentication() != null) {
            return SecurityContextHolder.getContext().getAuthentication().isAuthenticated();
        }
        return false;
    }

    @Override
    public boolean attemptLogin(String username, String password) {
        LOGGER.trace("#attemptLogin");
        LOGGER.debug("Attempt login by Authentication Manger: manager: {}, username: {}", authenticationManager, username);

        boolean result = false;
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
        WebAuthenticationDetailsSource source = new WebAuthenticationDetailsSource();
        token.setDetails(source.buildDetails(VaadinServletService.getCurrentServletRequest()));
        try {
            Authentication authResult = authenticationManager.authenticate(token);
            if (authResult.isAuthenticated()) {
                LOGGER.debug("Authentication Success: {}", authResult.getPrincipal());
                SecurityContextHolder.getContext().setAuthentication(authResult);
                result = authResult.isAuthenticated();
            }
        } catch (AuthenticationException e) {
            LOGGER.warn("Authentication FAILED: {}", e.getCause());
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public boolean attemptLogout() {
        LOGGER.debug("#attemptLogout:");
        SecurityContextHolder.getContext().setAuthentication(null);
        return true;
    }

    private void updateAccessGrants(SideBar.AbstractMenuItem item, Authentication authentication) {
        LOGGER.trace("#updateAccessGrants: item={}, authentication={}", item, authentication);
        // TODO:: Temporal static implementation for 'ROLE_GOD'.
        if (!authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_GOD")) && item.getAccessRoles().length > 0) {
            Set<ConfigAttribute> configs = new HashSet<>();
            for (String role : item.getAccessRoles()) {
                configs.add(new SecurityConfig(role));
            }
            try {
                accessDecisionManager.decide(authentication, null, configs);
                item.setAccessible(true);
            } catch (AccessDeniedException e) {
                item.setAccessible(false);
            }
        } else {
            item.setAccessible(true);
        }
        LOGGER.debug("MenuItem access control result: {}; name: {}, against: {}, permit: {}", item.isAccessible(), item.getCaption(), authentication.getPrincipal(), item.getAccessRoles());

        if (item.hasChildren()) {
            for (SideBar.AbstractMenuItem child : item.getChildren()) {
                updateAccessGrants(child, authentication);
            }
        }

    }

}
