package ext.david.ui.vaadin;

import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.internal.Conventions;
import ext.david.ui.vaadin.core.SideBar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class AnnotationSideBarMenuProvider implements SideBarMenuProvider, ApplicationContextAware, InitializingBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(AnnotationSideBarMenuProvider.class);
    private ApplicationContext applicationContext;
    private Set<SideBar.AbstractMenuItem> menuitems;

    public AnnotationSideBarMenuProvider() {
        LOGGER.trace("#CONSTRUCTOR");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        findAndCreateMenuItems();
    }

    private void findAndCreateMenuItems() {
        LOGGER.trace("#findAndCreateMenuItems");

        menuitems = new HashSet<>();

        String[] beansNames = applicationContext.getBeanNamesForAnnotation(SideBarContent.class);

        for (String beanName : beansNames) {

            SideBarContent annotation = applicationContext.findAnnotationOnBean(beanName, SideBarContent.class);
            String viewName = getViewNameFromVaadin(beanName);
            String caption = annotation.caption().isEmpty() ? viewName : annotation.caption();

            SideBar.MenuItem menuItem = new SideBar.MenuItem(caption, viewName, annotation.icon(), annotation.weight());
            menuItem.setAccessRoles(annotation.hasRoles());

            SideBarGroup groupAnnotation = annotation.group();
            if (!groupAnnotation.caption().isEmpty()) {
                SideBar.AbstractMenuItem menuGroup = findMenuItemGroup(groupAnnotation.caption()).orElseGet(() -> {
                    SideBar.MenuGroup group = new SideBar.MenuGroup(groupAnnotation.caption(), groupAnnotation.icon(), groupAnnotation.weight());
                    menuitems.add(group);
                    return group;
                });
                menuGroup.addChild(menuItem);
                LOGGER.trace("menu({}) added on group({})", menuItem, menuGroup);
            } else {
                menuitems.add(menuItem);
            }
        }
    }

    private Optional<SideBar.AbstractMenuItem> findMenuItemGroup(String groupNmae) {
        return menuitems.stream().filter(menuItem -> {
            return menuItem.isMenuGroup() && groupNmae.equals(menuItem.getCaption());
        }).findFirst();
    }

    private String getViewNameFromVaadin(String beanName) {
        SpringView springView = applicationContext.findAnnotationOnBean(beanName, SpringView.class);
        Class beanType = applicationContext.getType(beanName);
        return Conventions.deriveMappingForView(beanType, springView);
    }

    @Override
    public Set<SideBar.AbstractMenuItem> provideMenuItems() {
        return menuitems;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
