package ext.david.ui.vaadin;

import com.vaadin.shared.Registration;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

public class SimpleLogin extends HorizontalLayout {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleLogin.class);

    private String applicationName;

    private LoginForm loginForm;
    private LoginForm.LoginListener loginListener;
    private Registration registration;
    private VerticalLayout labelsContainer;
    private Label titleLabel;
    private Label descriptionLabel;

    public SimpleLogin() {
        init();
    }

    public void addLoginListener(LoginForm.LoginListener loginListener) {
        loginForm.addLoginListener(loginListener);
        this.loginListener = loginListener;
    }

    public void clearField() {
        LOGGER.trace("#clearField");
        rebuildLoginForm();
    }

    public void setTitle(String title) {
        titleLabel.setValue(title);

    }

    public void setDescription(String description) {
        descriptionLabel.setValue(description);
    }

    protected void setLoginBackgroundEnabled(boolean enabled) {
        if (enabled) {
            addStyleName("login-background");
        } else {
            removeStyleName("login-background");
        }
    }

    private void rebuildLoginForm() {
        removeComponent(loginForm);
        loginForm = new LoginForm();
        loginForm.addLoginListener(this.loginListener);
        addComponent(loginForm);
        setComponentAlignment(loginForm, Alignment.MIDDLE_RIGHT);
    }

    private void init() {
        loginForm = new LoginForm();
        titleLabel = new Label("application");
        titleLabel.addStyleName(ValoTheme.LABEL_H2);
        descriptionLabel = new Label("Description");
        labelsContainer = new VerticalLayout();
        labelsContainer.addComponents(titleLabel, descriptionLabel);
        addComponents(labelsContainer, loginForm);
        setComponentAlignment(labelsContainer, Alignment.MIDDLE_LEFT);
        setComponentAlignment(loginForm, Alignment.MIDDLE_RIGHT);
        setLoginBackgroundEnabled(true);
        setSizeFull();
    }


}
