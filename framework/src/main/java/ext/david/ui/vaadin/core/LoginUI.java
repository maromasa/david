package ext.david.ui.vaadin.core;

import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.LoginForm;
import com.vaadin.ui.UI;
import ext.david.ui.vaadin.SimpleLogin;
import ext.david.ui.vaadin.UILoginHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;

import java.util.Locale;

@SpringUI(path = "/login")
public class LoginUI extends UI implements LoginForm.LoginListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginUI.class);
    private final MessageSource messageSource;
    private final UILoginHandler uiLoginHandler;
    private SimpleLogin simpleLogin;

    public LoginUI(UILoginHandler uiLoginHandler, MessageSource messageSource) {
        LOGGER.trace("#CONSTRUCTOR: uiLoginHandler={}, MessageSource={}", uiLoginHandler, messageSource);
        this.messageSource = messageSource;
        this.uiLoginHandler = uiLoginHandler;
        simpleLogin = new SimpleLogin();
        setLocale(Locale.getDefault());
    }

    @Override
    public void onLogin(LoginForm.LoginEvent event) {
        boolean result = uiLoginHandler.attemptLogin(event.getLoginParameter("username"), event.getLoginParameter("password"));
        if (result) {
            getPage().setLocation("/");
        }
    }

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        LOGGER.trace("#init: request={}", vaadinRequest);
        simpleLogin.addLoginListener(this);
        simpleLogin.setTitle(messageSource.getMessage("login.title", null, getLocale()));
        simpleLogin.setTitle(messageSource.getMessage("login.description", null, getLocale()));
        if (getNavigator() != null) {
            getNavigator().addView("", Navigator.EmptyView.class);
        }
        setContent(simpleLogin);
    }

}
