package ext.david.ui.vaadin.view;

import com.vaadin.data.PropertySet;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import ext.david.file.FileProcessor;
import ext.david.file.FileWatchService;
import ext.david.ui.vaadin.AbstractApplicationView;
import ext.david.ui.vaadin.SideBarContent;

@SideBarContent
public class FileWatchServiceView extends AbstractApplicationView {

    private FileWatchService fileWatchService;
    private Button serviceEnabledButton;

    public FileWatchServiceView(FileWatchService fileWatchService) {
        super("File Watcher Service");
        this.fileWatchService = fileWatchService;
        initComponents();
    }

    private void initComponents() {

        HorizontalLayout topControls = new HorizontalLayout();
        serviceEnabledButton = new Button("Disable Service");
        serviceEnabledButton.addStyleName(ValoTheme.BUTTON_DANGER);
        serviceEnabledButton.addClickListener(clickEvent -> {
           if (fileWatchService.isRunning()) {
               fileWatchService.stop();
           } else {
               fileWatchService.start();
           }
        });
        topControls.addComponents(serviceEnabledButton);

        Grid<FileProcessor> grid = new Grid<>();
        addComponents(topControls, grid);
    }

    @Override
    protected void viewChanged(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        super.viewChanged(viewChangeEvent);
        if (fileWatchService.isRunning()) {
            serviceEnabledButton.setCaption("Disable Service");
        } else {
            serviceEnabledButton.setCaption("Enable Service");
        }
    }

    @Override
    protected AbstractOrderedLayout getContainerComponent() {
        return new VerticalLayout();
    }
}
