package ext.david.ui.vaadin;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import ext.david.model.core.User;
import ext.david.ui.vaadin.core.DefaultVaadinApplicationUI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractApplicationView extends VerticalLayout implements View {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractApplicationView.class);
    private DefaultVaadinApplicationUI parent;
    private Label captionLabel = new Label();
    private AbstractOrderedLayout container = getContainerComponent();

    public AbstractApplicationView(String caption) {
        init();
        setCaption(caption);
    }

    protected User getCurrentUser() {
//        return (this.parent != null) ? parent.getCurrentUser() : null;
        return null;
    }

    @Override
    public DefaultVaadinApplicationUI getParent() {
        return parent;
    }

    @Autowired
    public void setParent(DefaultVaadinApplicationUI parent) {
        LOGGER.trace("#setParent: {}", parent);
        this.parent = parent;
    }

    @Override
    public void addComponents(Component... components) {
        container.addComponents(components);
    }

    @Override
    public void setExpandRatio(Component component, float ratio) {
        container.setExpandRatio(component, ratio);
    }

    @Override
    public void addComponent(Component c) {
        container.addComponent(c);
    }

    @Override
    public void addComponentAsFirst(Component c) {
        container.addComponentAsFirst(c);
    }

    @Override
    public void addComponent(Component c, int index) {
        container.addComponent(c, index);
    }

    @Override
    public void removeComponent(Component c) {
        container.removeComponent(c);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        LOGGER.debug("#enter: class={}; event={}", getClass(), viewChangeEvent);
        viewChanged(viewChangeEvent);
    }

    @Override
    public String getCaption() {
        return captionLabel.getCaption();
    }

    @Override
    public void setCaption(String caption) {
        captionLabel.setValue(caption);
    }

    protected AbstractOrderedLayout getContainerComponent() {
        return new HorizontalLayout();
    }

    protected void viewChanged(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
    }

    private void init() {
        LOGGER.trace("#init(): {}", this);

        captionLabel.addStyleName(ValoTheme.LABEL_H2);
        container.setMargin(false);
        container.setSizeFull();

        super.addComponent(captionLabel);
        super.addComponent(container);
        super.setExpandRatio(container, 1);

        setMargin(false);
        setSizeFull();
    }

}
