package ext.david.file;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.integration.file.filters.FileListFilter;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class LegacyStatusFileFilter implements FileListFilter<File>, InitializingBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(LegacyStatusFileFilter.class);

    private String extension = "STS";
    private boolean caseSensitive = true;
    private Pattern pattern;

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public boolean isCaseSensitive() {
        return caseSensitive;
    }

    public void setCaseSensitive(boolean caseSensitive) {
        this.caseSensitive = caseSensitive;
    }

    @Override
    public List<File> filterFiles(File[] files) {
        if (pattern == null) throw new IllegalStateException("RexEx Pattern is not prepared.");
        List<File> result = new ArrayList<>();
        for (File file : files) {
            if (pattern.matcher(file.getName()).matches()) {
                LOGGER.debug("Matched file: {}", file);
                File dataFile = findDataFile(file);
                if (dataFile != null) {
                    result.add(dataFile);
                    LOGGER.info("Detected DATA file. Status file will be deleted: {}", file);
                    file.delete();
                } else {
                    LOGGER.warn("Could not detect DATA file against status file: {}", file.getAbsolutePath());
                }
            } else {
                LOGGER.trace("Unmatched RegEx: '{}'", file.getName());
            }
        }
        return result;
    }

    @Override
    public void afterPropertiesSet() {
        LOGGER.trace("#afterPropertiesSet");
        if (pattern == null) {
            String regex = String.format(".+\\.%s$", extension);
            pattern = caseSensitive ? Pattern.compile(regex) : Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
            LOGGER.info("Created RegEx Pattern: '{}'; extension='{}', caseSensitive: {}", pattern, extension, caseSensitive);
        }

    }

    private File findDataFile(File statusFile) {
        String statusFilename = statusFile.getName();
        if (statusFilename.indexOf(extension) > 0) {
            String basename = statusFilename.substring(0, statusFilename.indexOf(extension) - 1);
            File[] dataFiles = statusFile.getParentFile().listFiles(new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    if (pathname.getName().startsWith(basename) && !pathname.equals(statusFile)) {
                        LOGGER.debug("Found data file: {}; fromSTS={}", pathname, statusFilename);
                        return true;
                    } else {
                        return false;
                    }
                }
            });

            if (dataFiles != null && dataFiles.length > 0) {
                if (dataFiles.length > 1) {
                    LOGGER.warn("Detected more than 1 file. Always first index are returned.");
                }
                return dataFiles[0];
            }

        } else {
            LOGGER.warn("Illegal Situation: statusFilename={}, extension={}", statusFilename, extension);
        }
        return null;
    }

}
