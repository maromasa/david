package ext.david.file;


import ext.david.file.layout.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.charset.Charset;

public class FileLayoutParserFileProcessor extends AbstractFileProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileLayoutParserFileProcessor.class);

    private String layoutId;
    private FileLayout fileLayout;
    private boolean charsetNameSpecified = false;
    private FileLayoutParser fileLayoutParser = new FileLayoutParser();
    private FileLayoutParser.FormatType formatType = FileLayoutParser.FormatType.FIXED_LENGTH;

    public FileLayoutParserFileProcessor(FileLayoutParser fileLayoutParser) {
        this.fileLayoutParser = fileLayoutParser;
    }

    public FileLayoutParserFileProcessor() {
    }

    public FileLayoutParserFileProcessor(String layoutId) {
        this.layoutId = layoutId;
    }

    public FileLayoutParser.FormatType getFormatType() {
        return formatType;
    }

    public void setFormatType(FileLayoutParser.FormatType formatType) {
        this.formatType = formatType;
    }

    public FileLayoutParser getFileLayoutParser() {
        return fileLayoutParser;
    }

    public void setFileLayoutParser(FileLayoutParser fileLayoutParser) {
        this.fileLayoutParser = fileLayoutParser;
    }

    public String getLayoutId() {
        return layoutId;
    }

    public void setLayoutId(String layoutId) {
        this.layoutId = layoutId;
    }

    public void setParserConsumer(ParserConsumer parserConsumer) {
        fileLayoutParser.setParserConsumer(parserConsumer);
    }

    public FileLayout getFileLayout() {
        return fileLayout;
    }

    public void setFileLayout(FileLayout fileLayout) {
        LOGGER.info("Using layout: {}", fileLayout);
        this.fileLayout = fileLayout;
    }

    @Override
    public String getCharsetName() {
        if (charsetNameSpecified == false && fileLayoutParser.getFileLayout() != null) {
            return fileLayoutParser.getFileLayout().getDefaultCharset();
        } else {
            return super.getCharsetName();
        }
    }

    @Override
    public void setCharsetName(String charsetName) {
        this.charsetNameSpecified = true;
        super.setCharsetName(charsetName);
    }

    @Override
    protected void processImpl(File file) {
        prepareLayoutParser();
        Charset charset = getCharsetInstance();
        LOGGER.debug("Parsing file: encoding='{}': file='{}'", charset.displayName(), file.getAbsolutePath());
        fileLayoutParser.parseFile(file, formatType, charset);
    }

    private void prepareLayoutParser() {
        if (fileLayoutParser.getFileLayout() == null) {
            if (FileLayoutManager.hasLayout(layoutId)) {
                fileLayoutParser.setFileLayout(FileLayoutManager.getLayout(layoutId));
                return;
            } else {
                throw new IllegalStateException();
            }
        }
    }
}
