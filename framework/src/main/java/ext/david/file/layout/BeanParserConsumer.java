package ext.david.file.layout;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.util.ObjectUtils;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.Optional;

public class BeanParserConsumer<T> implements ParserConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(BeanParserConsumer.class);

    private final Class<T> beanClass;
    private BeanHandler<T> beanHandler;
    private PopulationResolver<T> populationResolver;

    public BeanParserConsumer(Class<T> beanClass) {
        this.beanClass = beanClass;
    }

    public BeanParserConsumer(Class<T> beanClass, BeanHandler<T> beanHandler, PopulationResolver<T> populationResolver) {
        this.beanClass = beanClass;
        this.beanHandler = beanHandler;
        this.populationResolver = populationResolver;
    }

    public PopulationResolver<T> getPopulationResolver() {
        return populationResolver;
    }

    public void setPopulationResolver(PopulationResolver<T> populationResolver) {
        this.populationResolver = populationResolver;
    }

    public void setBeanHandler(BeanHandler<T> beanHandler) {
        this.beanHandler = beanHandler;
    }

    @Override
    public void consumeParsedMap(Map<LayoutField, Object> parsedMap) {

        T bean = getBean(parsedMap);
        boolean populated = false;

        for (Map.Entry<LayoutField, Object> entry : parsedMap.entrySet()) {

            String propName = entry.getKey().getPropertyName();

            if (propName != null) {

                try {
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(propName, bean.getClass());
                    LOGGER.trace("Method: {}, Value: {}", propertyDescriptor.getWriteMethod(), entry.getValue());
                    propertyDescriptor.getWriteMethod().invoke(bean, entry.getValue());
                    populated = true;

                    if (populated = false && populationResolver != null) {
                        populationResolver.resolvePopulation(bean, entry.getKey(), entry.getValue());
                        populated = true;
                    }

                } catch (IntrospectionException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }

        }

        if (populated) consumeAsBean(bean);

    }

    private void consumeAsBean(T bean) {
        if (beanHandler != null) beanHandler.consumeAsBean(bean);
    }

    private T getBean(Map<LayoutField, Object> parsedMap) {

        if (beanClass == null && beanHandler == null) throw new IllegalStateException();

        T bean = null;

        try {

            if (beanHandler != null) {
                bean = beanHandler.newInstance(parsedMap);
            }

            bean = Optional.ofNullable(bean).orElse(beanClass.newInstance());

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } finally {
            if (bean == null) {
                throw new IllegalStateException("Could not create bean instance for mapping");
            }
        }

        return bean;

    }

    public interface PopulationResolver<T> {
        void resolvePopulation(T bean, LayoutField layoutField, Object value);
    }

    public interface BeanHandler<T> {

        T newInstance(Map<LayoutField, Object> parsedMap);

        void consumeAsBean(T bean);

    }

}
