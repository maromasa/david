package ext.david.file.layout;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FileLayout {

    private String id;
    private String name;
    private String description;
    private String defaultCharset = StandardCharsets.UTF_8.name();
    private String author;
    private Date created;
    private Date updated;
    private boolean multiRecord;

    private Map<String, LayoutRecord> recordMap = new LinkedHashMap<>();

    public FileLayout() {
    }

    public FileLayout(String id) {
        this.id = id;
        this.created = new Date();
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public String getDefaultCharset() {
        return defaultCharset;
    }

    public void setDefaultCharset(String defaultCharset) {
        this.defaultCharset = defaultCharset;
    }

    public Collection<LayoutRecord> getRecords() {
        return recordMap.values();
    }

    public void setRecords(Collection<LayoutRecord> records) {
        for (LayoutRecord record : records) {
            addRecord(record);
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isMultiRecord() {
        return multiRecord;
    }

    public int getRecordCount() {
        return recordMap.size();
    }

    public boolean hasRecord(String id) {
        return recordMap.containsKey(id);
    }

    public LayoutRecord getRecord(String id) {
        return hasRecord(id) ? recordMap.get(id) : null;
    }

    public LayoutRecord addRecord(LayoutRecord layoutRecord) {
        LayoutRecord added = recordMap.put(layoutRecord.getId(), layoutRecord);
        updateMultiRecord();
        if (!validateMultiRecord(layoutRecord)) {
            throw new IllegalArgumentException("KeyField MUST exists in Record when multi record layout.");
        }
        return added;
    }

    private boolean validateMultiRecord(LayoutRecord layoutRecord) {
        return multiRecord ? layoutRecord.hasKeyField() : true;
    }

    private void updateMultiRecord() {
        multiRecord = (recordMap.size() > 1);
    }

}
