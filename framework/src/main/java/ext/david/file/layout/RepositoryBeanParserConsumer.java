package ext.david.file.layout;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Map;

public class RepositoryBeanParserConsumer<T> extends BeanParserConsumer<T> {

    public RepositoryBeanParserConsumer(Class<T> beanClass, JpaRepository<T, ?> repository, PopulationResolver<T> populationResolver) {
        super(beanClass, new RepositoryBeanHandler<T>(beanClass, repository), populationResolver);
    }

    private static class RepositoryBeanHandler<T> implements BeanHandler<T> {

        private final JpaRepository<T, ?> repository;
        Class<T> beanClass;

        public RepositoryBeanHandler(Class<T> beanClass, JpaRepository<T, ?> repository) {
            this.beanClass = beanClass;
            this.repository = repository;
        }

        @Override
        public T newInstance(Map<LayoutField, Object> parsedMap) {
            try {
                return beanClass.newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public void consumeAsBean(T bean) {
            repository.save(bean);
        }

    }
}
