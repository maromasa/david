package ext.david.file.layout;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LayoutRecord {

    private String id;
    private String name;
    private long length;
    private String keyValue;
    private Map<String, LayoutField> fieldMap = new LinkedHashMap<>();

    public String getKeyValue() {
        return keyValue;
    }

    public void setKeyValue(String keyValue) {
        this.keyValue = keyValue;
    }

    public LayoutField getField(String id) {
        return fieldMap.containsKey(id) ? fieldMap.get(id) : null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LayoutField addField(LayoutField field) {
        if (field.getIndex() == 0) {
            field.setIndex(fieldMap.size() + 1);
        }
        return fieldMap.put(field.getId(), field);
    }

    public int getFieldCount() {
        return fieldMap.size();
    }

    public boolean hasField(String id) {
        return fieldMap.containsKey(id);
    }

    public boolean hasField(LayoutField field) {
        return fieldMap.containsValue(field);
    }

    public boolean hasKeyField() {
        return getKeyFields().size() > 0;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getLength() {
        return length;
    }

    public void setLength(long length) {
        this.length = length;
    }

    public Collection<LayoutField> getFields() {
        return fieldMap.values();
    }

    public void setFields(Collection<LayoutField> fields) {
        for (LayoutField layoutField : fields) {
            addField(layoutField);
        }
    }

    public Collection<LayoutField> getKeyFields() {
        return getFields().stream().filter(t -> (t instanceof LayoutKeyField || t.isRecordKey())).collect(Collectors.toList());
    }

}
