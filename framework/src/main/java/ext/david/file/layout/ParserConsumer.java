package ext.david.file.layout;

import java.util.Map;

public interface ParserConsumer {

    void consumeParsedMap(Map<LayoutField, Object> parsedMap);

}
