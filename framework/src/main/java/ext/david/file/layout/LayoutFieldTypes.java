package ext.david.file.layout;

public enum LayoutFieldTypes {

    TYPE_STRING,
    TYPE_NUMERIC,
    TYPE_DATE,
    TYPE_DATETIME
}
