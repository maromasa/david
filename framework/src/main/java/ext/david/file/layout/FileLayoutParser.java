package ext.david.file.layout;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import java.util.Map;

public class FileLayoutParser {

    public static final String DEFAULT_PARSER_CHARSET_NAME = "windows-31j";

    private static final Logger LOGGER = LoggerFactory.getLogger(FileLayoutParser.class);

    private FileLayout fileLayout;
    private Charset parserCharset;
    private ParserConsumer parserConsumer;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat();

    public ParserConsumer getParserConsumer() {
        return parserConsumer;
    }

    public void setParserConsumer(ParserConsumer parserConsumer) {
        this.parserConsumer = parserConsumer;
    }

    public Charset getParserCharset() {
        return parserCharset;
    }

    public void setParserCharset(Charset parserCharset) {
        this.parserCharset = parserCharset;
    }

    public FileLayout getFileLayout() {
        return fileLayout;
    }

    public void setFileLayout(FileLayout fileLayout) {
        this.fileLayout = fileLayout;
    }

    public void parseFile(File file) {
        parseFile(file, FormatType.FIXED_LENGTH);
    }

    public void parseFile(File file, FormatType formatType) {
        parseFile(file, formatType, Charset.defaultCharset());
    }

    public void parseFile(File file, FormatType formatType, Charset readerCharset) {

        validateObjects();
        AbstractLineParser lineParser = getLineParser(formatType);

        try (BufferedReader reader = Files.newBufferedReader(file.toPath(), readerCharset)) {

            reader.lines().forEach(lineString -> {

                lineParser.setLineString(lineString);
                Map<LayoutField, Object> parsedValues = parseLine(lineParser);

                if (parserConsumer != null) {
                    parserConsumer.consumeParsedMap(parsedValues);
                }

            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Map<LayoutField, Object> parseLine(String lineString, FormatType formatType) {
        AbstractLineParser lineParser = getLineParser(formatType);
        lineParser.setLineString(lineString);
        return parseLine(lineParser);
    }

    private Map<LayoutField, Object> parseLine(AbstractLineParser lineParser) {

        if (lineParser.getLineString() == null || lineParser.getLineString().isEmpty()) {
            throw new IllegalStateException();
        }

        LayoutRecord layoutRecord = determineLayoutRecord(lineParser);

        Map<LayoutField, Object> parsedValues = new LinkedHashMap<>(layoutRecord.getFields().size());

        for (LayoutField layoutField : layoutRecord.getFields()) {

            String loadedString = lineParser.parseFieldValue(layoutField);

            LOGGER.trace("parsed field[{}]: id='{}', value='{}'", layoutField.getIndex(), layoutField.getId(), loadedString);
            Object parsedValue = null;

            if (loadedString.trim().length() > 0) {

                switch (layoutField.getType()) {
                    case TYPE_STRING:
                        parsedValue = loadedString.trim();
                        break;

                    case TYPE_NUMERIC:
                        parsedValue = Long.valueOf(loadedString.trim());
                        break;

                    case TYPE_DATE:
                        if (layoutField.getFormatPattern() != null) {
                            simpleDateFormat.applyPattern(layoutField.getFormatPattern());
                            try {
                                parsedValue = simpleDateFormat.parse(loadedString.trim());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        } else {
                            LOGGER.warn("");
                        }
                        break;
                }
            }

            parsedValues.put(layoutField, parsedValue);

        }

        return parsedValues;
    }

    private AbstractLineParser getLineParser(FormatType formatType) {

        AbstractLineParser lineParser;
        switch (formatType) {
            case FIXED_LENGTH:
                prepareParserCharset();
                lineParser = new FixedLengthLineParser(parserCharset);
                break;
            case TSV:
                lineParser = new DelimitedLineParser("\t");
                break;
            case CSV:
                lineParser = new DelimitedLineParser(",");
                break;
            default:
                throw new IllegalStateException();
        }

        return lineParser;

    }

    private void validateObjects() {
        LOGGER.trace("#validateObjects");
    }

    private LayoutRecord determineLayoutRecord(AbstractLineParser lineParser) {

        validateObjects();
        LayoutRecord determined = null;

        if (fileLayout.isMultiRecord()) {

            for (LayoutRecord layoutRecord : fileLayout.getRecords()) {

                StringBuffer keyStringBuffer = new StringBuffer();
                for (LayoutField layoutField : layoutRecord.getKeyFields()) {
                    keyStringBuffer.append(lineParser.parseFieldValue(layoutField));
                }

                String keyValue = keyStringBuffer.toString();
                LOGGER.debug("Generated String Value for Layout Matching: '{}'", keyValue);

                if (keyValue.equals(layoutRecord.getKeyValue())) {
                    determined = layoutRecord;
                    LOGGER.debug("Matched: layout={}", layoutRecord.getId());
                    break;
                } else {
//                    LOGGER.trace("Unmatched layout: layout={}", layoutRecord.getId());
                }
            }

        } else {
            determined = fileLayout.getRecords().iterator().next();
        }
        return determined;
    }


    private void prepareParserCharset() {
        if (parserCharset == null) {
            parserCharset = Charset.availableCharsets().getOrDefault(DEFAULT_PARSER_CHARSET_NAME, Charset.defaultCharset());
            LOGGER.warn("Charset is not specified. using: {}", parserCharset);
        }
    }

    public enum FormatType {
        FIXED_LENGTH,
        TSV,
        CSV
    }

    public static abstract class AbstractLineParser {

        String lineString;

        public String getLineString() {
            return lineString;
        }

        public void setLineString(String lineString) {
            this.lineString = lineString;
        }

        abstract String parseFieldValue(LayoutField layoutField);

    }

    public static class DelimitedLineParser extends AbstractLineParser {

        private final String delimiter;

        public DelimitedLineParser(String delimiter) {
            this.delimiter = delimiter;
        }

        @Override
        String parseFieldValue(LayoutField layoutField) {
            String[] values = lineString.split(delimiter);
            if (values.length >= layoutField.getIndex()) {
                return values[layoutField.getIndex() - 1];
            }
            return null;
        }
    }

    public static class FixedLengthLineParser extends AbstractLineParser {

        private final Charset parserCharset;
        private byte[] lineBytes;

        public FixedLengthLineParser(Charset parserCharset) {
            this.parserCharset = parserCharset;
        }

        @Override
        public void setLineString(String lineString) {
            super.setLineString(lineString);
            this.lineBytes = lineString.getBytes(parserCharset);
        }

        @Override
        String parseFieldValue(LayoutField layoutField) {
            return new String(lineBytes, layoutField.getStartPosition() - 1, layoutField.getLength(), parserCharset);
        }

    }
}
