package ext.david.file.layout;

public class LayoutField {

    private int index;
    private String id;
    private String name;
    private int length;
    private String propertyName;
    private int startPosition;
    private LayoutFieldTypes type = LayoutFieldTypes.TYPE_STRING;
    private String formatPattern;
    private boolean recordKey = false;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getFormatPattern() {
        return formatPattern;
    }

    public void setFormatPattern(String formatPattern) {
        this.formatPattern = formatPattern;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getStartPosition() {
        return startPosition;
    }

    public void setStartPosition(int startPosition) {
        this.startPosition = startPosition;
    }

    public boolean isRecordKey() {
        return recordKey;
    }

    public void setRecordKey(boolean recordKey) {
        this.recordKey = recordKey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public LayoutFieldTypes getType() {
        return type;
    }

    public void setType(LayoutFieldTypes type) {
        this.type = type;
    }
}
