package ext.david.file.layout;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;

public class FileLayoutManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileLayoutManager.class);

    private static FileLayoutManager me = INSTANCE();

    private List<FileLayout> fileLayoutList = new ArrayList<>();
    private Map<String, FileLayout> fileLayoutMap = new HashMap<>();

    private FileLayoutManager() {
        LOGGER.trace("#CONSTRUCTOR");
    }

    public static FileLayoutManager INSTANCE() {
        if (me == null) {
            me = new FileLayoutManager();
        }
        return me;
    }

    public static Collection<FileLayout> getAll() {
        return Collections.unmodifiableCollection(me.fileLayoutMap.values());
    }

    public static boolean hasLayout(String id) {
        return me.fileLayoutMap.containsKey(id);
    }

    public static FileLayout getLayout(String id) {
        return (me.fileLayoutMap.containsKey(id)) ? me.fileLayoutMap.get(id) : null;
    }

    public static void importJSON(File jsonFile) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            List<FileLayout> fileLayouts = objectMapper.readValue(jsonFile, new TypeReference<List<FileLayout>>() {
            });
            for (FileLayout fileLayout : fileLayouts) {
                LOGGER.info("Appending FileLayout information: id={}", fileLayout.getId());
                me.fileLayoutMap.put(fileLayout.getId(), fileLayout);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * TOOD:: implement
     *
     * @param jsonFile
     */
    public static void exportJSON(File jsonFile) {

    }

    /**
     * @param outputStream
     */
    public static void exportJSON(OutputStream outputStream) {

    }

    public static FileLayoutBuilder builder(String id) {
        return new FileLayoutBuilder(id);
    }

    public void addLayout(FileLayout fileLayout) {
        fileLayoutMap.put(fileLayout.getId(), fileLayout);
    }

    public void removeLayout(String id) {
        if (fileLayoutMap.containsKey(id)) {
            fileLayoutMap.remove(id);
        }
    }

    public static final class FileLayoutBuilder {

        private FileLayout fileLayout;

        private FileLayoutBuilder(String id) {
            this.fileLayout = new FileLayout(id);
        }

        public FileLayoutBuilder name(String name) {
            fileLayout.setName(name);
            return this;
        }

        public FileLayoutBuilder description(String description) {
            fileLayout.setDescription(description);
            return this;
        }


        public FileLayout get() {
            INSTANCE().addLayout(fileLayout);
            return fileLayout;
        }

        public LayoutRecordBuilder record(String id) {
            return new LayoutRecordBuilder(this, id);
        }

    }

    public static final class LayoutRecordBuilder {

        private FileLayoutBuilder fileLayoutBuilder;
        private LayoutRecord layoutRecord;

        LayoutRecordBuilder(FileLayoutBuilder builder, String id) {
            this.fileLayoutBuilder = builder;
            this.layoutRecord = new LayoutRecord();
            layoutRecord.setId(id);
        }

        public LayoutRecordBuilder keyValue(String keyValue) {
            layoutRecord.setKeyValue(keyValue);
            return this;
        }

        public LayoutRecordBuilder length(long length) {
            layoutRecord.setLength(length);

            return this;
        }

        public LayoutFieldBuilder field(String id) {
            return new LayoutFieldBuilder(this, id, false);
        }

        public LayoutFieldBuilder keyField(String id) {
            return new LayoutFieldBuilder(this, id, true);
        }

        public FileLayoutBuilder and() {
            fileLayoutBuilder.fileLayout.addRecord(layoutRecord);
            return fileLayoutBuilder;
        }

        LayoutRecordBuilder andRecord(String id) {
            return this.and().record(id);
        }

        FileLayout andGet() {
            return this.and().get();
        }
    }

    public static final class LayoutFieldBuilder {

        private LayoutRecordBuilder layoutRecordBuilder;
        private LayoutField layoutField;

        public LayoutFieldBuilder(LayoutRecordBuilder layoutRecordBuilder, String id, boolean isKey) {
            this.layoutRecordBuilder = layoutRecordBuilder;
            layoutField = isKey ? new LayoutKeyField() : new LayoutField();
            layoutField.setId(id);
            layoutField.setRecordKey(isKey);
        }

        public LayoutFieldBuilder formatPattern(String formatPattern) {
            layoutField.setFormatPattern(formatPattern);
            return this;
        }

        public LayoutFieldBuilder name(String name) {
            layoutField.setName(name);
            return this;
        }

        public LayoutFieldBuilder length(int length) {
            layoutField.setLength(length);
            return this;
        }

        public LayoutFieldBuilder startPosition(int startPosition) {
            layoutField.setStartPosition(startPosition);
            return this;
        }

        public LayoutFieldBuilder type(LayoutFieldTypes type) {
            layoutField.setType(type);
            return this;
        }

        public LayoutRecordBuilder and() {
            layoutRecordBuilder.layoutRecord.addField(layoutField);
            return layoutRecordBuilder;
        }

        public LayoutRecordBuilder andRecord(String id) {
            return this.and().andRecord(id);
        }

        public FileLayout andGet() {
            return this.and().andGet();
        }
    }

    public static abstract class LayoutHierachryBuilderBese<O, B> {
        public abstract B name(String name);

        public abstract O get();
    }

}
