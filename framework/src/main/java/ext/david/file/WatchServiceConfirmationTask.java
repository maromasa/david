package ext.david.file;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.scheduling.TaskScheduler;

import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;

public class WatchServiceConfirmationTask implements Runnable, InitializingBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(WatchServiceConfirmationTask.class);

    private long delay;
    private long interval;
    private WatchService watchService;
    private WatchKey watchKey;
    private Path watchPath;
    private TaskScheduler taskScheduler;
    private List<Path> pendingPathList;

    public WatchServiceConfirmationTask() {
        pendingPathList = new ArrayList<>();
    }

    private void confirm() {
        LOGGER.info("#confirm invoked");

        for (WatchEvent<?> event : watchKey.pollEvents()) {
            LOGGER.info("WatchKey event detected: {}; kind:{}", event, event.kind());
            if (event.context() instanceof Path && event.kind() == StandardWatchEventKinds.ENTRY_CREATE) {
                Path path = (Path) event.context();
                LOGGER.info("Detects Path. Append to pendingList: {}; canWrite: {}", path, watchPath.resolve(path).toFile().canWrite());
                pendingPathList.add(path);

            }
        }

        List<Path> invokablePathList = new ArrayList<>();
        for (Path p : pendingPathList) {
            LOGGER.info("Cheking pending Path: {};", p);
            if (tryToWritable(p)) {
                LOGGER.info("Now pending file was readable: {}", p.toFile().getAbsolutePath());
                invokablePathList.add(p);
            }
        }

        if (invokablePathList.size() > 0) invokeAndConsumeList(invokablePathList);
    }

    private void invokeAndConsumeList(List<Path> list) {
        for (Path path : list) {
            pendingPathList.remove(path);
            LOGGER.info("invoke! {}", watchPath.resolve(path));
        }
    }


    private boolean tryToWritable(Path path) {
        boolean result = false;
        try {
            Path child = watchPath.resolve(path);
            LOGGER.info("Try to Open channel and tryLock() method: {}", child);
            FileChannel writeChannel = FileChannel.open(child, StandardOpenOption.WRITE);
            writeChannel.tryLock();
            writeChannel.close();
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
            result = false;
        } finally {

        }
        return result;
    }

    private void setup() {
        FileSystem fileSystem = FileSystems.getDefault();
        try {
            watchService = fileSystem.newWatchService();
            watchPath = fileSystem.getPath("./target");
            watchKey = watchPath.register(watchService, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_MODIFY);
        } catch (IOException e) {
            e.printStackTrace();
        }
        LOGGER.info("setup: Created WatchService and WatchKey instances: {}, {}", watchService, watchKey);
    }

    @Override
    public void run() {
        confirm();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        setup();
    }
}
