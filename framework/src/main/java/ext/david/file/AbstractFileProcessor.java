package ext.david.file;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public abstract class AbstractFileProcessor implements FileProcessor {

    public static final String DEFAULT_CHARSET_NAME = StandardCharsets.UTF_8.name();

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractFileProcessor.class);

    private String filenamePattern = ".*";
    private String charsetName = DEFAULT_CHARSET_NAME;
    private boolean active = true;

    @Override
    public void processFile(File file) {
        LOGGER.trace("#processFile: file={}, active={}", file, active);

        if (isActive()) {
            processImpl(file);
        } else {
            LOGGER.info("This processor is NOT 'active'. Nothing to do");
        }
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public void setActive(boolean active) {
        this.active = active;
    }

    public String getCharsetName() {
        return charsetName;
    }

    public void setCharsetName(String charsetName) {
        this.charsetName = charsetName;
    }

    public String getFilenamePattern() {
        return filenamePattern;
    }

    public void setFilenamePattern(String regex) {
        this.filenamePattern = regex;
    }

    @Override
    public boolean isProcessTarget(File file) {
        LOGGER.trace("#isProcessTarget: file={}", file);

        boolean eval = file.getName().matches(getFilenamePattern());
        LOGGER.debug("isProcessTarget evaluation: {}; fileName={}, regEx={}", eval, file.getName(), getFilenamePattern());
        return eval;
    }

    protected abstract void processImpl(File file);

    protected Charset getCharsetInstance() {
        String tryCharsetName = getCharsetName();
        if (Charset.availableCharsets().containsKey(tryCharsetName)) {
            return Charset.forName(tryCharsetName);
        } else {
            return StandardCharsets.UTF_8;
        }
    }
}
