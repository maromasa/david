package ext.david.file;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.Lifecycle;
import org.springframework.integration.endpoint.SourcePollingChannelAdapter;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
public class SpringIntegrationFileWatchService implements FileWatchService, MessageHandler, Lifecycle, InitializingBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(SpringIntegrationFileWatchService.class);

    private SourcePollingChannelAdapter sourcePollingChannelAdapter;
    private List<FileProcessor> processors = new ArrayList<>();
    private boolean enableBackup;
    private File backupDir;
    private FileWatchServiceLogger serviceLogger;

    public SpringIntegrationFileWatchService(SourcePollingChannelAdapter sourcePollingChannelAdapter) {
        this.sourcePollingChannelAdapter = sourcePollingChannelAdapter;
        this.serviceLogger = new InternalLoggingFileWatchServiceLogger();
    }

    public boolean isEnableBackup() {
        return enableBackup;
    }

    public void setEnableBackup(boolean enableBackup) {
        this.enableBackup = enableBackup;
    }

    public File getBackupDir() {
        return backupDir;
    }

    public void setBackupDir(File backupDir) {
        this.backupDir = backupDir;
    }

    @Override
    public Collection<FileProcessor> getProcessors() {
        return processors;
    }

    @Autowired(required = false)
    public void setProcessors(List<FileProcessor> processors) {
        LOGGER.trace("#setProcessors: processors={}", processors);
        this.processors = processors;
    }

    @Override
    public void addFileProcessor(FileProcessor fileProcessor) {
        processors.add(fileProcessor);
    }

    @Override
    public void removeFileProcessor(FileProcessor fileProcessor) {
        processors.remove(fileProcessor);
    }

    @Override
    public void start() {
        serviceLogger.serviceStart();
        sourcePollingChannelAdapter.start();
    }

    @Override
    public void stop() {
        serviceLogger.serviceStop();
        sourcePollingChannelAdapter.stop();
    }

    @Override
    public boolean isRunning() {
        return sourcePollingChannelAdapter.isRunning();
    }

    @Override
    public void handleMessage(Message<?> message) throws MessagingException {
        LOGGER.trace("#handleMessage: {}", message);

        if (message.getPayload() instanceof File) {

            Message<File> fileMessage = (Message<File>) message;
            File file = fileMessage.getPayload();

            FileProcessor processor = detectFileProcessor(file);

            if (processor != null) {

                if (enableBackup) {
                    backupFile(file);
                }

                if (serviceLogger != null) {
                    serviceLogger.processStart(file, processor);
                }

                processor.processFile(file);

                if (serviceLogger != null) {
                    serviceLogger.processEnd(file, processor);
                }

                LOGGER.info("File Processing have complete. Delete this file: {}", file.getAbsolutePath());
                file.delete();

            } else {

                LOGGER.warn("No suitable FileProcessors are matched. Nothing TODO: File='{}'", file);

            }
        } else {
            LOGGER.warn("Unsupported Message Payload: {}", message.getPayload());
        }

    }

    @Override
    public void afterPropertiesSet() throws Exception {
        LOGGER.trace("#afterPropertiesSet");
        if (enableBackup) {
            if (backupDir == null) {
                LOGGER.warn("backupDir is not specified. Disable backup");
                enableBackup = false;
            } else {
                if (!backupDir.exists()) {
                    backupDir.mkdirs();
                    LOGGER.info("Backup Dir is not exist. Creating: '{}'", backupDir.getAbsolutePath());
                }
            }

        }
    }

    private void backupFile(File file) {
        LOGGER.trace("#backupFile: file={}", file);
        Date date = new Date(System.currentTimeMillis());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd.HHmmss");
        Path source = file.toPath();
        Path destination = backupDir.toPath().resolve(String.format("%s.%s", file.getName(), simpleDateFormat.format(date)));
        try {
            Files.copy(source, destination);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private FileProcessor detectFileProcessor(File file) {
        FileProcessor matchedProcessor = null;
        for (FileProcessor processor : processors) {
            if (processor.isProcessTarget(file)) {
                LOGGER.debug("Detected processor: {}", processor);
                matchedProcessor = processor;
            } else {
                LOGGER.debug("FileProcessor [{}] is not matched for: {}", processor, file);
            }
        }
        return matchedProcessor;
    }

    private static class InternalLoggingFileWatchServiceLogger implements FileWatchServiceLogger {

        @Override
        public void serviceStart() {
            LOGGER.info("File Watch Service Started.");
        }

        @Override
        public void processDisabled(FileProcessor fileProcessor) {


        }

        @Override
        public void processEnabled(FileProcessor fileProcessor) {

        }

        @Override
        public void processStart(File file, FileProcessor fileProcessor) {
            LOGGER.info("File Process started: file={}, processor={}", file, fileProcessor);
        }

        @Override
        public void processEnd(File file, FileProcessor fileProcessor) {
            LOGGER.info("File Process End: file={}, processor={}", file, fileProcessor);
        }

        @Override
        public void processFailed(File file, FileProcessor fileProcessor) {

        }

        @Override
        public void serviceStop() {
            LOGGER.info("File Watch Service Stoped.");

        }
    }
}
