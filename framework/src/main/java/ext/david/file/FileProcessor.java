package ext.david.file;

import java.io.File;

public interface FileProcessor {

    boolean isActive();

    void setActive(boolean active);

    boolean isProcessTarget(File file);

    void processFile(File file);


}
