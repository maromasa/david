package ext.david.file;

import java.util.Collection;

public interface FileWatchService {

    void addFileProcessor(FileProcessor fileProcessor);

    void removeFileProcessor(FileProcessor fileProcessor);

    Collection<FileProcessor> getProcessors();

    void start();

    void stop();

    boolean isRunning();

}
