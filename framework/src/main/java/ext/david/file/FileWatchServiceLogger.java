package ext.david.file;

import java.io.File;

public interface FileWatchServiceLogger {

    void serviceStart();

    void processDisabled(FileProcessor fileProcessor);

    void processEnabled(FileProcessor fileProcessor);

    void processStart(File file, FileProcessor fileProcessor);

    void processEnd(File file, FileProcessor fileProcessor);

    void processFailed(File file, FileProcessor fileProcessor);

    void serviceStop();

}
