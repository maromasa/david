package ext.david.config;

public abstract class AbstractWebMvcApplication {

    protected abstract void configure(DavidApplicationConfigurationProperties properties);
    
}
