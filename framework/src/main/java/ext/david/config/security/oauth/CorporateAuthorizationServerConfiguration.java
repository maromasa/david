package ext.david.config.security.oauth;

import ext.david.security.oauth.domain.ManagedApplicationRepository;
import ext.david.security.oauth.service.ManagedApplicationManager;
import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.*;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

@Configuration
public class CorporateAuthorizationServerConfiguration {

    @Bean
    public ResourceServerConfigurer resourceServerConfigurer(ResourceServerProperties properties) {
        return new OAuth2ResourceServerConfigurer(properties);
    }

    public static class OAuth2ResourceServerConfigurer extends ResourceServerConfigurerAdapter {

        private ResourceServerProperties resourceServerProperties;

        public OAuth2ResourceServerConfigurer(ResourceServerProperties resourceServerProperties) {
            this.resourceServerProperties = resourceServerProperties;
        }

        //        private final ResourceServerTokenServices tokenServices;

//        public OAuth2ResourceServerConfigurer(ResourceServerTokenServices tokenServices) {
//            this.tokenServices = tokenServices;
//        }

        @Override
        public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
//            resources.tokenServices(tokenServices);
        }

        @Override
        public void configure(HttpSecurity http) throws Exception {
            resourceServerProperties.getTokenInfoUri();
            http.antMatcher("/me").authorizeRequests().anyRequest().authenticated();

        }
    }


    @Configuration
    @EnableAuthorizationServer
    @EnableResourceServer
    public static class OAuth2AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {

        private final ManagedApplicationManager applicationManager;
        private final AuthenticationManager authenticationManager;

        public OAuth2AuthorizationServerConfiguration(ManagedApplicationManager applicationManager, AuthenticationManager authenticationManager) {
            this.applicationManager = applicationManager;
            this.authenticationManager = authenticationManager;
        }

        @Override
        public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
            security.checkTokenAccess("authenticated");
            security.tokenKeyAccess("authenticated");
            security.allowFormAuthenticationForClients();
        }

        @Override
        public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
            clients.withClientDetails(applicationManager);
        }

        @Override
        public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
            endpoints.authenticationManager(authenticationManager);

        }
    }

    @Configuration
    @EnableJpaRepositories(basePackageClasses = {ManagedApplicationRepository.class})
    public static class ClientDetailsServiceConfiguration {

        @Bean
        public ManagedApplicationManager applicationManager(ManagedApplicationRepository applicationRepository) {
            ManagedApplicationManager applicationManager = new ManagedApplicationManager(applicationRepository);
            return applicationManager;
        }
    }
}
