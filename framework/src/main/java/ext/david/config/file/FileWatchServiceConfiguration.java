package ext.david.config.file;

import ext.david.file.FileWatchService;
import ext.david.file.LegacyStatusFileFilter;
import ext.david.file.SpringIntegrationFileWatchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.annotation.Poller;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.aop.AbstractMessageSourceAdvice;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.config.SourcePollingChannelAdapterFactoryBean;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.endpoint.AbstractPollingEndpoint;
import org.springframework.integration.endpoint.SourcePollingChannelAdapter;
import org.springframework.integration.file.FileReadingMessageSource;
import org.springframework.integration.file.config.FileReadingMessageSourceFactoryBean;
import org.springframework.integration.scheduling.PollSkipAdvice;
import org.springframework.integration.scheduling.PollSkipStrategy;
import org.springframework.integration.scheduling.PollerMetadata;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.scheduling.support.PeriodicTrigger;

import java.io.File;

@Configuration
@EnableIntegration
public class FileWatchServiceConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileWatchServiceConfiguration.class);

    @Value("${david.file.watchDir:./watch}")
    private String watchDir = "./watch";

    @Value("${david.file.watchInterval:3000}")
    private int watchInterval = 3000;

    @Value("${david.file.watchInitialDelay:10000}")
    private int watchInitialDelay = 10000;

    @Value("${david.file.watchAutoStartup:false}")
    private boolean autoStartup = false;

    @Value("${david.file.enableBackup:true}")
    private boolean enableBackup = true;

    @Value("${david.file.backupDir:./backup}")
    private String backupDir = "./backup";

    @Value("${david.file.legacyFilter.enabled:true}")
    private boolean legacyFileFilter = true;

    @Value("${david.file.legacyFilter.extension:STS}")
    private String legacyFileFilterExtension = "STS";

    @Value("${david.file.legacyFilter.caseSensitive:false}")
    private boolean legacyFileFilterCaseSensitive = false;

    @Bean
    public MessageChannel inputFileChannel() {
        return new DirectChannel();
    }

    @Bean
    public SourcePollingChannelAdapterFactoryBean sourcePollingChannelAdapterFactoryBean() {

        FileReadingMessageSource messageSource = new FileReadingMessageSource();
        messageSource.setAutoCreateDirectory(true);
        messageSource.setUseWatchService(true);

        File watchFile = new File(watchDir);
        messageSource.setDirectory(watchFile);

        if (legacyFileFilter) {
            LegacyStatusFileFilter legacyStatusFileFilter = new LegacyStatusFileFilter();
            legacyStatusFileFilter.setExtension(legacyFileFilterExtension);
            legacyStatusFileFilter.setCaseSensitive(legacyFileFilterCaseSensitive);
            legacyStatusFileFilter.afterPropertiesSet();
            messageSource.setFilter(legacyStatusFileFilter);
        }


        SourcePollingChannelAdapterFactoryBean bean = new SourcePollingChannelAdapterFactoryBean();
        bean.setSource(messageSource);
        bean.setPollerMetadata(pollerMetadata());
        bean.setOutputChannel(inputFileChannel());
        bean.setAutoStartup(autoStartup);
        messageSource.afterPropertiesSet();
        return bean;
    }

    @Bean(name = PollerMetadata.DEFAULT_POLLER_METADATA_BEAN_NAME)
    public PollerMetadata pollerMetadata() {
        PollerMetadata pollerMetadata = new PollerMetadata();
        PeriodicTrigger periodicTrigger = new PeriodicTrigger(watchInterval);
        periodicTrigger.setInitialDelay(watchInitialDelay);
        pollerMetadata.setTrigger(periodicTrigger);
        return pollerMetadata;
    }

    @Bean
    @ServiceActivator(inputChannel = "inputFileChannel")
    public FileWatchService fileWatchProcessService(SourcePollingChannelAdapter sourcePollingChannelAdapter) {
        SpringIntegrationFileWatchService watchService = new SpringIntegrationFileWatchService(sourcePollingChannelAdapter);
        watchService.setEnableBackup(enableBackup);
        watchService.setBackupDir(new File(backupDir));
        return watchService;
    }

}
