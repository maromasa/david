package ext.david.config.report;

import ext.david.report.jasperreports.JasperReportsController;
import ext.david.report.jasperreports.JasperReportsReportService;
import ext.david.report.jasperreports.ReportServiceViewResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;

@Configuration
public class JasperReportsConfiguration {

    private String prefix = "classpath:/reports/";
    private String suffix = ".jrxml";
    private int order = 0;

    @Bean
    public JasperReportsController jasperReportController() {
        return new JasperReportsController();
    }

    @Bean
    public JasperReportsReportService jasperReportsReportService() {
        return new JasperReportsReportService();
    }

	@Bean
	public ViewResolver jasperReportsViewResolver() {
		ReportServiceViewResolver jasperReportsViewResolver = new ReportServiceViewResolver(jasperReportsReportService());
		jasperReportsViewResolver.setPrefix(prefix);
		jasperReportsViewResolver.setSuffix(suffix);
		jasperReportsViewResolver.setOrder(order);
		return jasperReportsViewResolver;
	}

}
