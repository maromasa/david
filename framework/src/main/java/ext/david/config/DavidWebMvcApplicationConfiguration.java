package ext.david.config;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DavidWebMvcApplicationConfiguration {

    @Bean
    public DavidApplicationConfigurationProperties davidApplicationConfigurationProperties() {
        System.out.println("Creates davidApplicationConfigurationProperties");
        DavidApplicationConfigurationProperties properties = new DavidApplicationConfigurationProperties();
        return properties;
    }

    @Bean
    public BeanPostProcessor davidApplicationConfigurationBeanPostProcessor() {
        return new DavidApplicationConfiguraitonBeanPostProcesor();
    }

    public static class DavidApplicationConfiguraitonBeanPostProcesor implements BeanPostProcessor, ApplicationContextAware {

        private ApplicationContext applicationContext;

        @Override
        public Object postProcessBeforeInitialization(Object o, String s) throws BeansException {
            if (o instanceof DavidApplicationConfigurationProperties) {
                System.out.println("postProcessBeforeInitialization 'DavidApplicationConfigurationProperties' bean: " + o);
            }
            return o;
        }

        @Override
        public Object postProcessAfterInitialization(Object o, String s) throws BeansException {
            if (o instanceof DavidApplicationConfigurationProperties) {
                DavidApplicationConfigurationProperties properties = (DavidApplicationConfigurationProperties) o;

                // updateing Annotation on ApplicationClass.
                for (String beanName : applicationContext.getBeanNamesForAnnotation(DavidWebMvcApplication.class)) {
                    DavidWebMvcApplication annotation = applicationContext.findAnnotationOnBean(beanName, DavidWebMvcApplication.class);
                    if (annotation != null) {
                        properties.setMultiUser(annotation.multiUser());
                        properties.setOauth2SingleSignOn(annotation.oauthSingleSignOn());
                    }
                }

                System.out.println("postProcessAfterInitialization 'DavidApplicationConfigurationProperties' bean: " + properties);

            }
            return o;
        }

        @Override
        public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
            this.applicationContext = applicationContext;

        }
    }

}
