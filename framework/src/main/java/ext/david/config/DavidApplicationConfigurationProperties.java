package ext.david.config;

public class DavidApplicationConfigurationProperties {

    private String name;
    private boolean multiUser = true;
    private boolean oauth2SingleSignOn = false;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isMultiUser() {
        return multiUser;
    }

    public void setMultiUser(boolean multiUser) {
        this.multiUser = multiUser;
    }

    public boolean isOauth2SingleSignOn() {
        return oauth2SingleSignOn;
    }

    public void setOauth2SingleSignOn(boolean oauth2SingleSignOn) {
        this.oauth2SingleSignOn = oauth2SingleSignOn;
    }

}
