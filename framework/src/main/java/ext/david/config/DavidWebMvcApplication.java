package ext.david.config;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@SpringBootApplication
@Import(value = {DavidWebMvcApplicationConfiguration.class})
public @interface DavidWebMvcApplication {

    boolean multiUser() default true;

    boolean oauthSingleSignOn() default false;

}
