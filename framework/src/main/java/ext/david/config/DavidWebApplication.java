package ext.david.config;

import ext.david.app.Application;
import ext.david.app.web.ApplicationController;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DavidWebApplication {

    @Bean
    public Application application() {
        return new Application();
    }

    @Bean
    public ApplicationController applicationController() {
        return new ApplicationController();
    }

}
