package ext.david.misc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import java.util.concurrent.Executors;

public class ApplicationScheduler implements SchedulingConfigurer {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationScheduler.class);

    private ScheduledTaskRegistrar scheduledTaskRegistrar;

    @Scheduled(fixedRate = 5000)
    public void execute() {
        System.out.println("executed!");
        LOGGER.trace("{}, {}, {}", scheduledTaskRegistrar, scheduledTaskRegistrar.getScheduler(), scheduledTaskRegistrar.getFixedRateTaskList());
    }

    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        this.scheduledTaskRegistrar = taskRegistrar;
        scheduledTaskRegistrar.setScheduler(Executors.newSingleThreadScheduledExecutor());
    }
}
