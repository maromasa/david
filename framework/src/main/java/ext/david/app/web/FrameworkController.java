package ext.david.app.web;

import ext.david.app.Application;
import ext.david.app.Preference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.stereotype.Controller;

import java.util.Properties;

@Controller
public abstract class FrameworkController implements InitializingBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(FrameworkController.class);

    protected Application application;

    protected Properties userProperties;
    protected Properties applicationProperties;

    public Application getApplication() {
        return application;
    }

    @Autowired
    public void setApplication(Application application) {
        this.application = application;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        LOGGER.trace("#afterPropertiesSet: class={}", getClass());
        if (application != null) {
        }
    }
}
