package ext.david.app.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * The framework basic request handler such as "/about", "/status", etc
 */
@Controller
public class ApplicationController extends FrameworkController {

    @ResponseBody
    @RequestMapping(path = "/hello", method = RequestMethod.GET)
    public String helloWorld() {
        return String.format("Hello, This is %s", application.getName());
    }

    @RequestMapping(path = {"/", "/index"})
    public String index() {
        return "index";
    }

}
