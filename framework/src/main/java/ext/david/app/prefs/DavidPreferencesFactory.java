package ext.david.app.prefs;


import ext.david.app.web.ApplicationController;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.ui.context.support.UiApplicationContextUtils;

import java.util.prefs.Preferences;
import java.util.prefs.PreferencesFactory;

public class DavidPreferencesFactory implements PreferencesFactory {

    public DavidPreferencesFactory() {
        super();
    }

    @Override
    public Preferences systemRoot() {
        return null;
    }

    @Override
    public Preferences userRoot() {
        return null;
    }


    public enum PREFERENCES_TYPE {
        SYSTEM,
        USER
    }

}
