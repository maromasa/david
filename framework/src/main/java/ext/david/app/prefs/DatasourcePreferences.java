package ext.david.app.prefs;

import java.util.prefs.AbstractPreferences;
import java.util.prefs.BackingStoreException;

public class DatasourcePreferences extends AbstractPreferences {

    public DatasourcePreferences(DavidPreferencesFactory.PREFERENCES_TYPE type) {
        super(null, type.name());
    }

    @Override
    protected void putSpi(String key, String value) {
    }

    @Override
    protected String getSpi(String key) {
        return null;
    }

    @Override
    protected void removeSpi(String key) {
    }

    @Override
    protected void removeNodeSpi() throws BackingStoreException {
    }

    @Override
    protected String[] keysSpi() throws BackingStoreException {
        return new String[0];
    }

    @Override
    protected String[] childrenNamesSpi() throws BackingStoreException {
        return new String[0];
    }

    @Override
    protected AbstractPreferences childSpi(String name) {
        return null;
    }

    @Override
    protected void syncSpi() throws BackingStoreException {

    }

    @Override
    protected void flushSpi() throws BackingStoreException {

    }
}
