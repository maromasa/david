package ext.david.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.util.Assert;

public class Application implements InitializingBean, ApplicationContextAware, EnvironmentAware {

    protected static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    protected ApplicationContext applicationContext;
    protected Environment environment;

    private String id;
    private String name;
    private String version;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getVersion() {
        return version;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        LOGGER.trace("#afterPropertiesSet");
        Assert.notNull(applicationContext, "ApplicationContext is NULL");
        Assert.notNull(environment, "Environment is NULL");
    }

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

}
