package ext.david.app;

public interface PreferencePersistenceProvider {

    void persist(Preference preference);
}
