package ext.david.report.jasperreports;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(path = "/report")
public class JasperReportsController {

    @Autowired
    private JasperReportsReportService jasperReportsReportService;

    @RequestMapping(path = "/{reportName}")
    public ModelAndView view(@PathVariable String reportName) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(reportName);
        return modelAndView;
    }

}
