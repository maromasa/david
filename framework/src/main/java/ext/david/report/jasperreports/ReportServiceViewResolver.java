package ext.david.report.jasperreports;

import ext.david.report.ReportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.view.AbstractUrlBasedView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

public class ReportServiceViewResolver extends UrlBasedViewResolver {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReportServiceViewResolver.class);

    private ReportService reportService;

    public ReportServiceViewResolver(ReportService reportService) {
        LOGGER.trace("#CONSTRUCTOR");
        this.reportService = reportService;
        setViewClass(requiredViewClass());
    }

    @Override
    protected Class<?> requiredViewClass() {
        return ReportServiceView.class;
    }

    @Override
    protected AbstractUrlBasedView buildView(String viewName) throws Exception {
        ReportServiceView view = (ReportServiceView) super.buildView(viewName);
        view.setReportService(reportService);
        return view;
    }
}
