package ext.david.report.jasperreports;

import ext.david.report.ReportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.view.AbstractUrlBasedView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;
import java.util.Map;

public class ReportServiceView extends AbstractUrlBasedView {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReportServiceView.class);
    private ReportService reportService;

    public ReportService getReportService() {
        return reportService;
    }

    public void setReportService(ReportService reportService) {
        this.reportService = reportService;
    }

    @Override
    public boolean checkResource(Locale locale) throws Exception {
        return getApplicationContext().getResource(getUrl()).exists();
    }

    @Override
    protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        LOGGER.trace("#renderMergedOutputModel: model={}, url={}", model, getUrl());
        response.setContentType("application/pdf");
        reportService.renderReport(getUrl(), response.getOutputStream());
    }

}
