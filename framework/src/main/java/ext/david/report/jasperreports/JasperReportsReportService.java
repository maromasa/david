package ext.david.report.jasperreports;

import com.sun.javafx.collections.UnmodifiableListSet;
import ext.david.report.ReportException;
import ext.david.report.ReportFormats;
import ext.david.report.ReportService;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.Resource;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class JasperReportsReportService implements ReportService, ApplicationContextAware {

    private static final Logger LOGGER = LoggerFactory.getLogger(JasperReportsReportService.class);

    private ApplicationContext applicationContext;
    private DataSource dataSource;

    @Autowired(required = false)
    public void setDataSource(DataSource dataSource) {
        LOGGER.debug("Injected Datasource: {}", dataSource);
        this.dataSource = dataSource;
    }

    @Override
    public void renderReport(String template, OutputStream outputStream) throws ReportException {
        renderImpl(template, outputStream, ReportFormats.FORMAT_PDF);
    }

    @Override
    public void renderReport(String template, OutputStream outputStream, ReportFormats reportFormat) throws ReportException {
        renderImpl(template, outputStream, reportFormat);

    }

    @Override
    public void renderReport(String template, ResultSet resultSet, OutputStream outputStream, ReportFormats reportFormat) throws ReportException {

    }

    @Override
    public Set<ReportFormats> supports() {
        return new UnmodifiableListSet<>(Arrays.asList(ReportFormats.FORMAT_PDF));
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    private void renderImpl(String template, OutputStream outputStream, ReportFormats reportFormat) throws ReportException {

        Resource reportTemplate = applicationContext.getResource(template);

        if (reportTemplate != null && reportTemplate.exists()) {

            try {

                JasperFillManager jasperFillManager = JasperFillManager.getInstance(DefaultJasperReportsContext.getInstance());

                JasperReport jasperReport;
                JasperPrint jasperPrint;

                Map<String, Object> params = new HashMap<>();
                params.put(JRParameter.REPORT_MAX_COUNT, 20);
                if (reportTemplate.getFilename().endsWith(".jasper")) {
                    jasperPrint = JasperFillManager.fillReport(reportTemplate.getInputStream(), params);
                } else if (reportTemplate.getFilename().endsWith(".jrxml")) {
                    LOGGER.debug("fill: {}", dataSource);
                    jasperPrint = JasperFillManager.fillReport(JasperCompileManager.compileReport(reportTemplate.getInputStream()), params, dataSource.getConnection());
                } else {
                    throw new ReportException();
                }


                JRAbstractExporter exporter;

                exporter = new JRPdfExporter();

                exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputStream));

                exporter.exportReport();


            } catch (JRException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            LOGGER.debug("provided template was not exists: {}", template);
        }


    }
}
