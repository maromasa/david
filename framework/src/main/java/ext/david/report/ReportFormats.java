package ext.david.report;

public enum ReportFormats {

    FORMAT_HTML,
    FORMAT_PDF,
    FORMAT_XML
}
