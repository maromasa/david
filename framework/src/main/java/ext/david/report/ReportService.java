package ext.david.report;

import java.io.OutputStream;
import java.sql.ResultSet;
import java.util.Set;

public interface ReportService {

    void renderReport(String template, OutputStream outputStream) throws ReportException;

    void renderReport(String template, OutputStream outputStream, ReportFormats reportFormat) throws ReportException;

    void renderReport(String template, ResultSet resultSet, OutputStream outputStream, ReportFormats reportFormat) throws ReportException;

    Set<ReportFormats> supports();

}
