package ext.david.boot.autoconfigure.security;

import ext.david.security.DataJpaUserAccountManager;
import ext.david.security.config.DataJpaUserAccountAuthenticationConfigurerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

@Configuration
@ConditionalOnBean(DataJpaUserAccountManager.class)
public class DataJpaAuthenticationConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataJpaAuthenticationConfiguration.class);

    public DataJpaAuthenticationConfiguration() {
        LOGGER.trace("#(AUTOCONFIG)CONSTRUCTED");
    }

    @Bean
    @Order(DavidSecurityProperties.AUTH_PROVIDER_DATAJPA_ORDER)
    public DataJpaUserAccountAuthenticationConfigurerAdapter dataJpaUserAccountAuthenticationConfigurerAdapter(DataJpaUserAccountManager userAccountManager) {
        DataJpaUserAccountAuthenticationConfigurerAdapter adapter = new DataJpaUserAccountAuthenticationConfigurerAdapter(userAccountManager);
        return adapter;
    }

}
