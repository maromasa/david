package ext.david.boot.autoconfigure.security;

import ext.david.model.core.account.UserAccount;
import ext.david.security.InMemoryUserAccountManager;
import ext.david.security.config.InMemoryUserAccountAuthenticationConfigurerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

@Configuration
public class InMemoryAuthenticationConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(InMemoryAuthenticationConfiguration.class);
    private final DavidSecurityProperties davidSecurityProperties;

    public InMemoryAuthenticationConfiguration(DavidSecurityProperties davidSecurityProperties) {
        LOGGER.trace("(AUTOCONFIG)CONSTRUCTED");
        this.davidSecurityProperties = davidSecurityProperties;
    }

    @Bean
    public InMemoryUserAccountManager inMemoryUserAccountManager() {
        return new InMemoryUserAccountManager();
    }

    @Bean
    @Order(DavidSecurityProperties.AUTH_PROVIDER_INMEMORY_ORDER)
    public InMemoryUserAccountAuthenticationConfigurerAdapter inMemoryUserAccountAuthenticationConfigurerAdapter(InMemoryUserAccountManager inMemoryUserAccountManager) {

        DavidSecurityProperties.God god = davidSecurityProperties.getGod();
        UserAccount godAccount = UserAccount.withUser(god.getName()).password(god.getPassword()).roles(god.getRoles()).build();
        InMemoryUserAccountAuthenticationConfigurerAdapter adapter = new InMemoryUserAccountAuthenticationConfigurerAdapter(inMemoryUserAccountManager);
        adapter.setGodAccount(godAccount);

        LOGGER.info("Configured 'GOD' as name='{}', password='{}'", god.getName(), god.getPassword());
        return adapter;
    }
}
