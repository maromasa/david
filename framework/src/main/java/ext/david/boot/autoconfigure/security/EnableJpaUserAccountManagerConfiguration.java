package ext.david.boot.autoconfigure.security;

import ext.david.model.core.account.UserAccount;
import ext.david.model.core.account.UserAccountRepository;
import ext.david.security.DataJpaUserAccountManager;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EntityScan(basePackageClasses = {UserAccount.class})
@EnableJpaRepositories(basePackageClasses = {UserAccountRepository.class})
public class EnableJpaUserAccountManagerConfiguration {

    @Bean
    public DataJpaUserAccountManager dataJpaUserDetailsManager(UserAccountRepository userAccountRepository) {
        return new DataJpaUserAccountManager(userAccountRepository);
    }

}
