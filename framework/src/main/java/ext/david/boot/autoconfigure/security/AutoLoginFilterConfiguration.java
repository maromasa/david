package ext.david.boot.autoconfigure.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Configuration
@Profile(value = {"dev", "development"})
public class AutoLoginFilterConfiguration {

    @Bean
    public GenericFilterBean autoLoginFilter(AuthenticationManager authenticationManager, DavidSecurityProperties securityProperties) {
        return new AutoLoginFilter(authenticationManager, securityProperties);
    }

    public static class AutoLoginFilter extends GenericFilterBean {

        private static final Logger LOGGER = LoggerFactory.getLogger(AutoLoginFilter.class);

        private final AuthenticationManager authenticationManager;
        private final DavidSecurityProperties davidSecurityProperties;
        private String username;
        private String password;
        private boolean executed = false;

        public AutoLoginFilter(AuthenticationManager authenticationManager, DavidSecurityProperties davidSecurityProperties) {
            LOGGER.trace("#CONSTRUCTOR");
            this.authenticationManager = authenticationManager;
            this.davidSecurityProperties = davidSecurityProperties;
        }

        @Override
        public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (authentication == null || !authentication.isAuthenticated() && executed == false) {
                LOGGER.info("Trying to preset loggin... for user: {}", this.username);
                UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
                WebAuthenticationDetailsSource source = new WebAuthenticationDetailsSource();
                token.setDetails(source.buildDetails((HttpServletRequest) servletRequest));
                Authentication result = authenticationManager.authenticate(token);
                LOGGER.debug("Authentication Result: {}", result);
                if (result != null && result.isAuthenticated()) {
                    SecurityContextHolder.getContext().setAuthentication(result);
                    executed = true;
                }
            }
            filterChain.doFilter(servletRequest, servletResponse);
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        @Override
        protected void initFilterBean() throws ServletException {
            LOGGER.trace("#initFilterBean()");
            if (username == null || password == null) {
                this.username = davidSecurityProperties.getGod().getName();
                this.password = davidSecurityProperties.getGod().getPassword();
            }
        }

    }

}
