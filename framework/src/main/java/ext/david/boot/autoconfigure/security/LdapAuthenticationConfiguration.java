package ext.david.boot.autoconfigure.security;

import ext.david.security.DataJpaUserAccountManager;
import ext.david.security.ldap.DavidUserDetailsContextMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.ldap.core.support.BaseLdapPathContextSource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.authentication.configurers.ldap.LdapAuthenticationProviderConfigurer;
import org.springframework.security.ldap.DefaultSpringSecurityContextSource;
import org.springframework.security.ldap.userdetails.UserDetailsContextMapper;
import org.springframework.util.StringUtils;

import java.util.Optional;

@Configuration
@ConditionalOnClass({BaseLdapPathContextSource.class, UserDetailsContextMapper.class})
@ConditionalOnProperty(prefix = "david.security.ldap", name = "url", matchIfMissing = false)
public class LdapAuthenticationConfiguration {

    public static final Logger LOGGER = LoggerFactory.getLogger(LdapAuthenticationConfiguration.class);

    private DavidSecurityProperties.Ldap ldap;

    public LdapAuthenticationConfiguration(DavidSecurityProperties davidSecurityProperties) {
        LOGGER.trace("(CONFIG)CONSTRUCTOR");
        this.ldap = davidSecurityProperties.getLdap();
    }

    @Bean
    @ConditionalOnMissingBean
    public BaseLdapPathContextSource ldapAuthenticationContextSource() {
        DefaultSpringSecurityContextSource contextSource = new DefaultSpringSecurityContextSource(ldap.getUrl());
        if (StringUtils.hasLength(ldap.getUsername())) {
            contextSource.setUserDn(ldap.getUsername());
            contextSource.setPassword(ldap.getPassword());
        }
        if (StringUtils.hasLength(ldap.getBase())) {
            contextSource.setBase(ldap.getBase());
        }
        return contextSource;
    }

    @Bean
    public LdapAuthenticationProviderConfigurer ldapAuthenticationProviderConfigurer(BaseLdapPathContextSource contextSource, Optional<DataJpaUserAccountManager> dataJpaUserManager) {
        DavidUserDetailsContextMapper userDetailsContextMapper = new DavidUserDetailsContextMapper(contextSource);
        LdapAuthenticationProviderConfigurer configurer = new LdapAuthenticationProviderConfigurer();
        configurer
                .contextSource(contextSource)
                .userDnPatterns(ldap.getUserDnPatterns())
                .userDetailsContextMapper(userDetailsContextMapper);
        if (dataJpaUserManager.isPresent()) {
            userDetailsContextMapper.setUserAccountManager(dataJpaUserManager.get());
        }
        return configurer;
    }

    @Configuration
    @Order(DavidSecurityProperties.AUTH_PROVIDER_LDAP_ORDER)
    public static class LdapAuthenticationConfigurerAdapter extends GlobalAuthenticationConfigurerAdapter {

        private static final Logger LOGGER = LoggerFactory.getLogger(LdapAuthenticationConfigurerAdapter.class);

        private LdapAuthenticationProviderConfigurer ldapAuthenticationProviderConfigurer;

        public LdapAuthenticationConfigurerAdapter(LdapAuthenticationProviderConfigurer ldapAuthenticationProviderConfigurer) {
            LOGGER.trace("#CONSTRUCTOR");
            this.ldapAuthenticationProviderConfigurer = ldapAuthenticationProviderConfigurer;
        }

        @Override
        public void init(AuthenticationManagerBuilder auth) throws Exception {
            LOGGER.trace("#init");
            auth.apply(ldapAuthenticationProviderConfigurer);
        }
    }
}
