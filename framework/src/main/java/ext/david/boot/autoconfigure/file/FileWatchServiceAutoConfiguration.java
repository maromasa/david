package ext.david.boot.autoconfigure.file;

import ext.david.config.file.FileWatchServiceConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.config.SourcePollingChannelAdapterFactoryBean;

@Configuration
@ConditionalOnClass({EnableIntegration.class, SourcePollingChannelAdapterFactoryBean.class})
@EnableConfigurationProperties(DavidFileProperties.class)
@Import(FileWatchServiceConfiguration.class)
public class FileWatchServiceAutoConfiguration {
}
