package ext.david.boot.autoconfigure;

import ext.david.report.ReportService;
import ext.david.report.jasperreports.JasperReportsController;
import ext.david.report.jasperreports.JasperReportsReportService;
import ext.david.report.jasperreports.ReportServiceViewResolver;
import net.sf.jasperreports.engine.JasperFillManager;
import org.eclipse.birt.report.model.parser.ReportState;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;

@Configuration
@ConditionalOnClass(JasperFillManager.class)
public class JasperReportsAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public ReportService jasperReportsReportService() {
        JasperReportsReportService jasperReportsReportService = new JasperReportsReportService();
        return jasperReportsReportService;
    }

    @Configuration
    @ConditionalOnWebApplication
    public static class JasperReportsWebConfiguration {

        @Bean
        public ViewResolver jasperReportsViewResolve(ReportService reportService) {
            ReportServiceViewResolver viewResolver = new ReportServiceViewResolver(reportService);
            viewResolver.setPrefix("classpath:/reports/");
            viewResolver.setSuffix(".jrxml");
            viewResolver.setOrder(0);
            return viewResolver;
        }

        @Bean
        public JasperReportsController jasperReportsController() {
            return new JasperReportsController();
        }
    }

}
