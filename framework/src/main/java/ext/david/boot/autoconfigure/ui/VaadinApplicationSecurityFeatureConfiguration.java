package ext.david.boot.autoconfigure.ui;

import com.vaadin.spring.annotation.UIScope;
import com.vaadin.spring.annotation.ViewScope;
import ext.david.ui.vaadin.SpringSecurityUILoginHandler;
import ext.david.ui.vaadin.UILoginHandler;
import ext.david.ui.vaadin.core.DefaultVaadinApplicationUI;
import ext.david.ui.vaadin.core.LoginUI;
import ext.david.ui.vaadin.view.JobManagerView;
import ext.david.ui.vaadin.view.SystemSettingsView;
import ext.david.ui.vaadin.view.UserManagerView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfiguration;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@AutoConfigureAfter(SecurityAutoConfiguration.class)
@ConditionalOnProperty(prefix = "david.security", name = "enabled", havingValue = "true", matchIfMissing = true)
@ConditionalOnBean(AuthenticationManager.class)
public class VaadinApplicationSecurityFeatureConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(VaadinApplicationSecurityFeatureConfiguration.class);

    public VaadinApplicationSecurityFeatureConfiguration() {
        LOGGER.trace("(CONFIG)#CONSTRUCTOR: {}", getClass());
    }

    @Bean
    @UIScope
    public LoginUI loginUI(UILoginHandler uiLoginHandler, MessageSource messageSource) {
        return new LoginUI(uiLoginHandler, messageSource);
    }

    @Bean
    public UILoginHandler uiLoginHandler(AuthenticationManager authenticationManager) {
        UILoginHandler uiLoginHandler = new SpringSecurityUILoginHandler(authenticationManager);
        return uiLoginHandler;
    }

    @Bean
    public SecurityFeatureInjector securityFeatureInjector(UILoginHandler uiLoginHandler) {
        return new SecurityFeatureInjector(uiLoginHandler);
    }

    @Bean
    @ConditionalOnClass(WebSecurityConfiguration.class)
    public WebSecurityConfigurerAdapter vaadinWebSecurityCofigurerAdapter() {
        return new VaadinWebSecurityConfigurerAdapter();
    }

    @Bean
    @ViewScope
    public SystemSettingsView systemSettingsView() {
        return new SystemSettingsView();
    }

    @Bean
    @ViewScope
    @ConditionalOnBean(ThreadPoolTaskScheduler.class)
    public JobManagerView jobManagerView(ThreadPoolTaskScheduler threadPoolTaskScheduler) {
        JobManagerView jobManagerView = new JobManagerView();
        jobManagerView.setThreadPoolTaskScheduler(threadPoolTaskScheduler);
        return jobManagerView;
    }

    @Bean
    @ViewScope
    public UserManagerView userManagerView() {
        return new UserManagerView();
    }

    protected static class VaadinWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {
        @Override
        protected void configure(HttpSecurity http) throws Exception {
            LOGGER.trace("(CONFIG)#configure: http={}", http);
            LOGGER.info("TRUN ON http Security Configuration of Spring Security for Vaadin Application");
            http.httpBasic().disable();
            http.anonymous().disable();
            http.csrf().disable();
        }
    }

    protected class SecurityFeatureInjector implements BeanPostProcessor {

        private final UILoginHandler uiLoginHandler;

        public SecurityFeatureInjector(UILoginHandler uiLoginHandler) {
            this.uiLoginHandler = uiLoginHandler;
        }

        @Override
        public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
            return bean;
        }

        @Override
        public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
            if (bean instanceof DefaultVaadinApplicationUI) {
                LOGGER.debug("Injected UILoginHandler on VaadinUI.");
                DefaultVaadinApplicationUI ui = (DefaultVaadinApplicationUI) bean;
                ui.setUiLoginHandler(uiLoginHandler);
            }
            return bean;
        }
    }
}
