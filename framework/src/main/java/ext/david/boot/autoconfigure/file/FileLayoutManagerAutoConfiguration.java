package ext.david.boot.autoconfigure.file;

import ext.david.file.layout.FileLayoutManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnClass(FileLayoutManager.class)
@EnableConfigurationProperties(DavidFileProperties.class)
public class FileLayoutManagerAutoConfiguration {

    private final DavidFileProperties davidFileProperties;

    public FileLayoutManagerAutoConfiguration(DavidFileProperties davidFileProperties) {
        this.davidFileProperties = davidFileProperties;
    }

    @Bean
    public FileLayoutLoader fileLayoutLoader() {
        FileLayoutLoader loader = new FileLayoutLoader();
        loader.setPrefix(davidFileProperties.getLayoutPrefix());
        loader.setSuffix(davidFileProperties.getLayoutSuffix());
        return loader;
    }

}
