package ext.david.boot.autoconfigure.file;

import ext.david.file.layout.FileLayoutManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.Resource;

import java.io.IOException;

public class FileLayoutLoader implements InitializingBean, ApplicationContextAware {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileLayoutLoader.class);

    private String prefix = "classpath*:/filelayout/";
    private String suffix = ".layout.json";
    private ApplicationContext applicationContext;

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        loadLayoutFiles();
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    private void loadLayoutFiles() {
        LOGGER.debug("#loadLayoutFiles: prefix={}; suffix={}", prefix, suffix);
        try {
            Resource[] resources = applicationContext.getResources(String.join("*", prefix, suffix));
            for (Resource resource : resources) {
                LOGGER.info("Loading for FileLayoutManger: file='{}'", resource.getFile());
                FileLayoutManager.importJSON(resource.getFile());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
