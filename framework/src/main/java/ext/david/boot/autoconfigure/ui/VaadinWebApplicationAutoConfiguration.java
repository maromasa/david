package ext.david.boot.autoconfigure.ui;

import com.vaadin.spring.annotation.SpringViewDisplay;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.spring.boot.VaadinAutoConfiguration;
import ext.david.ui.vaadin.AnnotationSideBarMenuProvider;
import ext.david.ui.vaadin.core.ApplicationUIConfiguration;
import ext.david.ui.vaadin.core.DefaultVaadinApplicationUI;
import ext.david.ui.vaadin.core.SideBar;
import ext.david.ui.vaadin.core.ViewContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnWebApplication
@ConditionalOnClass(VaadinAutoConfiguration.class)
public class VaadinWebApplicationAutoConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(VaadinWebApplicationAutoConfiguration.class);

    public VaadinWebApplicationAutoConfiguration() {
        LOGGER.trace("(CONFIG)#CONSTRUCTOR: this={}", getClass());
    }

    @Bean
    public AnnotationSideBarMenuProvider annotationSideBarMenuProvider() {
        return new AnnotationSideBarMenuProvider();
    }

    @Bean
    public ApplicationUIConfiguration applicationUIConfiguration() {
        return new ApplicationUIConfiguration();
    }

    @Bean
    @UIScope
    public DefaultVaadinApplicationUI vaadinApplicationUI(MessageSource messageSource) {
        DefaultVaadinApplicationUI vaadinApplicationUI = new DefaultVaadinApplicationUI();
        vaadinApplicationUI.setApplicationUIConfiguration(applicationUIConfiguration());
        vaadinApplicationUI.setMessageSource(messageSource);
        return vaadinApplicationUI;
    }

    @Bean
    @UIScope
    public SideBar sideBar() {
        SideBar sideBar = new SideBar();
        return sideBar;
    }

    @Bean
    @UIScope
    @SpringViewDisplay
    public ViewContent viewContent() {
        ViewContent viewContent = new ViewContent();
        return viewContent;
    }


}
