package ext.david.boot.autoconfigure.file;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "david.file")
public class DavidFileProperties {

    private String layoutPrefix = "classpath*:/filelayout/";
    private String layoutSuffix = "*.layout.json";
    private String watchDir = "./watch";
    private int watchInterval = 50000;
    private int watchInitialDelay = 100000;

    public String getLayoutSuffix() {
        return layoutSuffix;
    }

    public void setLayoutSuffix(String layoutSuffix) {
        this.layoutSuffix = layoutSuffix;
    }

    public String getLayoutPrefix() {
        return layoutPrefix;
    }

    public void setLayoutPrefix(String layoutPrefix) {
        this.layoutPrefix = layoutPrefix;
    }

    public String getWatchDir() {
        return watchDir;
    }

    public void setWatchDir(String watchDir) {
        this.watchDir = watchDir;
    }

    public int getWatchInterval() {
        return watchInterval;
    }

    public void setWatchInterval(int watchInterval) {
        this.watchInterval = watchInterval;
    }

    public int getWatchInitialDelay() {
        return watchInitialDelay;
    }

    public void setWatchInitialDelay(int watchInitialDelay) {
        this.watchInitialDelay = watchInitialDelay;
    }

}
