package ext.david.boot.autoconfigure.security;

import ext.david.model.core.account.UserAccount;
import ext.david.security.AuthenticationEventLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.ldap.LdapAutoConfiguration;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;

@Configuration
@ConditionalOnClass({AuthenticationManager.class, GlobalAuthenticationConfigurerAdapter.class, UserAccount.class})
@ConditionalOnProperty(prefix = "david.security", name = "enabled", havingValue = "true", matchIfMissing = true)
@Import({InMemoryAuthenticationConfiguration.class, DataJpaAuthenticationConfiguration.class, LdapAuthenticationConfiguration.class})
@AutoConfigureBefore({LdapAutoConfiguration.class, SecurityAutoConfiguration.class})
public class DavidAuthenticationAutoConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(DavidAuthenticationAutoConfiguration.class);

    public DavidAuthenticationAutoConfiguration() {
        LOGGER.trace("(CONFIG)CONSTRUCTED");
    }

    @Bean
    @ConditionalOnMissingBean
    public static DavidSecurityProperties davidSecurityProperties() {
        LOGGER.trace("(AUTOCONFIG)#davidSecurityProperties");
        return new DavidSecurityProperties();
    }

    @Bean
    public AuthenticationEventLogger authenticationEventLogger() {
        return new AuthenticationEventLogger();
    }
}
