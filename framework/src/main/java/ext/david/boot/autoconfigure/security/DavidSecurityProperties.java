package ext.david.boot.autoconfigure.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.UUID;

@ConfigurationProperties(prefix = "david.security")
public class DavidSecurityProperties {

    public static final int AUTH_PROVIDER_INMEMORY_ORDER = 0 - 10;
    public static final int AUTH_PROVIDER_DATAJPA_ORDER = AUTH_PROVIDER_INMEMORY_ORDER - 10;
    public static final int AUTH_PROVIDER_LDAP_ORDER = AUTH_PROVIDER_DATAJPA_ORDER - 10;
    private static final Logger LOGGER = LoggerFactory.getLogger(DavidSecurityProperties.class);
    private boolean enabled = true;
    private Ldap ldap = new Ldap();
    private God god = new God();

    public DavidSecurityProperties() {
        LOGGER.trace("#CONSTRUTED");
    }

    public God getGod() {
        return god;
    }

    public Ldap getLdap() {
        return ldap;
    }

    public void setLdap(Ldap ldap) {
        this.ldap = ldap;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public static class God {
        String name = "GOD";
        String password = UUID.randomUUID().toString();
        String roles = "ROLE_GOD";

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getRoles() {
            return roles;
        }

        public void setRoles(String roles) {
            this.roles = roles;
        }
    }

    public static class Ldap {
        String url;
        String userDnPatterns = "uid={}";
        String username;
        String password;
        String base;
        boolean autoUpdate = true;
        boolean autoCreate = true;

        public String getBase() {
            return base;
        }

        public void setBase(String base) {
            this.base = base;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getUserDnPatterns() {
            return userDnPatterns;
        }

        public void setUserDnPatterns(String userDnPatterns) {
            this.userDnPatterns = userDnPatterns;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public boolean isAutoCreate() {
            return autoCreate;
        }

        public void setAutoCreate(boolean autoCreate) {
            this.autoCreate = autoCreate;
        }

        public boolean isAutoUpdate() {
            return autoUpdate;
        }

        public void setAutoUpdate(boolean autoUpdate) {
            this.autoUpdate = autoUpdate;
        }
    }
}
