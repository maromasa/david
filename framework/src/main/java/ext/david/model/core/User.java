package ext.david.model.core;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class User implements UserDetails {

    @Id
    private String username;
    private String password;
    private String email;
    private boolean enabled;

    @ElementCollection(fetch = FetchType.EAGER)
    private Set<String> roles;

    protected User() {
        roles = new HashSet<>();
    }

    private User(String username, String password) {
        this();
        this.username = username;
        this.password = password;
    }

    public static Builder withUser(String username) {
        Builder b = new Builder();
        return b.username(username);
    }

    public boolean addRole(String s) {
        return roles.add(s);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toSet());
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.enabled;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.enabled;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.enabled;
    }

    @Override
    public boolean isEnabled() {
        return this.enabled;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean removeRole(Object o) {
        return roles.remove(o);
    }

    public static class Builder {

        private String username;
        private String password;
        private String email;
        private Set<String> roles;

        private Builder() {
            roles = new HashSet<>();
        }

        public User build() {
            User u = new User(this.username, this.password);
            u.password = this.password;
            u.email = this.email;
            u.roles = roles;
            u.enabled = true;
            return u;
        }

        public Builder email(String email) {
            this.email = email;
            return this;
        }

        public Builder password(String passowrd) {
            this.password = passowrd;
            return this;
        }

        public Builder roles(String... roles) {
            for (String role : roles) {
                this.roles.add(role);
            }
            return this;
        }

        public Builder roles(Set<String> roles) {
            this.roles = roles;
            return this;
        }

        public Builder username(String username) {
            this.username = username;
            return this;
        }

    }
}
