package ext.david.model.core.account;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public interface UserAccount {

    static UserAccountBuilder withUser(String username) {
        UserAccountBuilder builder = new UserAccountBuilder(username);
        return builder;
    }

    String getUsername();

    String getEmailAddress();

    void setEmailAddress(String emailAddress);

    Set<String> getRoles();

    boolean isEnabled();

    void setEnabled(boolean enabled);

    UserPreference getUserPreference();

    public static class UserAccountBuilder {

        String username;
        String password = "changeit";
        String emailAddress;
        Set<String> roles = new HashSet<>();
        boolean enabled = true;

        private UserAccountBuilder(String username) {
            this.username = username;
        }

        public UserAccountBuilder username(String username) {
            this.username = username;
            return this;
        }

        public UserAccountBuilder password(String password) {
            this.password = password;
            return this;
        }

        public UserAccountBuilder emailAddress(String emailAddress) {
            this.emailAddress = emailAddress;
            return this;
        }

        public UserAccountBuilder roles(String... roles) {
            this.roles.addAll(Arrays.asList(roles));
            return this;
        }

        public UserAccount build() {
            UserAccountImpl userAccount = new UserAccountImpl(this.username, this.password, this.roles);
            userAccount.setEmailAddress(this.emailAddress);
            userAccount.setEnabled(this.enabled);
            return userAccount;
        }

    }

}
