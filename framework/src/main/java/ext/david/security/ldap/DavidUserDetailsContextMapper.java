package ext.david.security.ldap;

import ext.david.model.core.account.UserAccount;
import ext.david.security.AbstractUserAccountManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.BaseLdapPathContextSource;
import org.springframework.ldap.query.LdapQueryBuilder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.ldap.userdetails.UserDetailsContextMapper;

import java.util.Collection;

/**
 * Auto-creation support when LDAP Authentication using.
 */
public class DavidUserDetailsContextMapper implements UserDetailsContextMapper {

    private static final Logger LOGGER = LoggerFactory.getLogger(DavidUserDetailsContextMapper.class);

    private final LdapTemplate ldapTemplate;
    private AbstractUserAccountManager userAccountManager;
    private boolean autoPersist = true;
    private boolean autoUpdate = true;

    public DavidUserDetailsContextMapper(BaseLdapPathContextSource authenticationContextSource) {
        this.ldapTemplate = new LdapTemplate(authenticationContextSource);
    }

    public AbstractUserAccountManager getUserAccountManager() {
        return userAccountManager;
    }

    public void setUserAccountManager(AbstractUserAccountManager userAccountManager) {
        this.userAccountManager = userAccountManager;
    }

    public boolean isAutoPersist() {
        return autoPersist;
    }

    public void setAutoPersist(boolean autoPersist) {
        this.autoPersist = autoPersist;
    }

    public boolean isAutoUpdate() {
        return autoUpdate;
    }

    public void setAutoUpdate(boolean autoUpdate) {
        this.autoUpdate = autoUpdate;
    }

    @Override
    public UserDetails mapUserFromContext(DirContextOperations ctx, String username, Collection<? extends GrantedAuthority> authorities) {
        LOGGER.trace("#mapUserFromContext");

        // TODO:: Issue, why DirContextOperations in arguments(ctx) could not get any attributes such as "email"?
        DirContextOperations dirContextOperations = ldapTemplate.searchForContext(LdapQueryBuilder.query().where("uid").is(username));

        UserDetails userDetails = null;

        if (userAccountManager != null) {

            if (userAccountManager.userExists(username)) {

                // The case which already exists in UserDetails
                userDetails = userAccountManager.loadUserByUsername(username);
                updateByLdapContext(dirContextOperations, (UserAccount) userDetails);
                if (autoUpdate) {
                    userAccountManager.updateUser(userDetails);
                }

            } else {

                UserAccount userAccount = UserAccount.withUser("username").roles("ROLE_USER").build();
                updateByLdapContext(dirContextOperations, userAccount);
                if (autoPersist) {
                    userAccountManager.createUser(userAccount);
                }
                userDetails = (UserDetails) userAccount;

            }

        } else {

            // when userDetailsManager is not exists, simply create User
            UserAccount userAccount = UserAccount.withUser("username").roles("ROLE_USER").build();
            updateByLdapContext(ctx, userAccount);
            userDetails = (UserDetails) userAccount;
        }

        return userDetails;

    }

    @Override
    public void mapUserToContext(UserDetails user, DirContextAdapter ctx) {
        throw new UnsupportedOperationException();
    }


    private void updateByLdapContext(DirContextOperations operations, UserAccount userAccount) {
        // update user entity information by LDAP context information.
        // TODO currently only email. consider some attributes should be include, such as 'organization' on User as 'Model'
        userAccount.setEmailAddress(operations.getStringAttribute("mail"));
    }

}
