package ext.david.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AbstractAuthenticationEvent;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;

public class AuthenticationEventLogger implements ApplicationListener<AbstractAuthenticationEvent> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationEventLogger.class);

    @Override
    public void onApplicationEvent(AbstractAuthenticationEvent event) {
        LOGGER.trace("onApplicationEvent: {}", event);
        if (event instanceof AuthenticationSuccessEvent) {
            processOnAuthenticationSuccess((AuthenticationSuccessEvent) event);
        }
    }

    private void processOnAuthenticationSuccess(AuthenticationSuccessEvent event) {
        Authentication authentication = event.getAuthentication();
        Object principal = authentication.getPrincipal();
        Object userDetails = authentication.getDetails();
        LOGGER.info("Authentication Success: Principal: {}; Details: {}", principal, userDetails);


    }

}
