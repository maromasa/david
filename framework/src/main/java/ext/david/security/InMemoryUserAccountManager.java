package ext.david.security;

import ext.david.model.core.account.UserAccount;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.HashMap;
import java.util.Map;

public class InMemoryUserAccountManager extends AbstractUserAccountManager {

    private Map<String, UserAccount> users = new HashMap<>();

    @Override
    public void deleteUser(String username) {
        if (users.containsKey(username)) {
            users.remove(username);
        }
    }

    @Override
    public void changePassword(String oldPassword, String newPassword) {

    }

    @Override
    public boolean userExists(String username) {
        return users.containsKey(username);
    }


    @Override
    public UserDetails loadUserByUsername(String username) {
        if (userExists(username)) {
            return (UserDetails) users.get(username);
        } else {
            throw new UsernameNotFoundException(username);
        }
    }

    @Override
    public void createUser(UserAccount userAccount) {
        users.put(userAccount.getUsername(), userAccount);
    }

    @Override
    public void updateUser(UserAccount userAccount) {
        users.put(userAccount.getUsername(), userAccount);

    }
}
