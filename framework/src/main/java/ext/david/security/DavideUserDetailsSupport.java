package ext.david.security;

import ext.david.model.core.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.provisioning.UserDetailsManager;

public abstract class DavideUserDetailsSupport implements UserDetailsManager {

    @Override
    public void createUser(UserDetails user) {
        if (!validateType(user)) {
            throw new UnsupportedOperationException();
        }
        createUser((User) user);
    }

    @Override
    public void updateUser(UserDetails user) {
        if (!validateType(user)) {
            throw new UnsupportedOperationException();
        }
        updateUser((User) user);
    }

    public abstract boolean userExists(String username);

    public abstract void createUser(User user);

    public User getUser(String username) {
        UserDetails u = loadUserByUsername(username);
        if (u instanceof User) {
            return (User) u;
        } else {
            return null;
        }
    }

    @Override
    public abstract UserDetails loadUserByUsername(String username);

    public abstract void updateUser(User user);

    protected boolean validateType(UserDetails userDetails) {
        return (userDetails instanceof User);
    }
}
