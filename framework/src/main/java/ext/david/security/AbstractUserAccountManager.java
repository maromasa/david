package ext.david.security;

import ext.david.model.core.account.UserAccount;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.provisioning.UserDetailsManager;

import java.util.Optional;

public abstract class AbstractUserAccountManager implements UserDetailsManager {

    @Override
    public void createUser(UserDetails user) {
        supports(user).ifPresent(userAccount -> createUser(userAccount));
    }

    public abstract void createUser(UserAccount userAccount);

    @Override
    public void updateUser(UserDetails user) {
        supports(user).ifPresent(userAccount -> updateUser(userAccount));
    }

    public abstract void updateUser(UserAccount userAccount);

    protected Optional<UserAccount> supports(UserDetails userDetails) {
        return Optional.ofNullable((userDetails instanceof UserAccount) ? (UserAccount) userDetails : null);
    }

}
