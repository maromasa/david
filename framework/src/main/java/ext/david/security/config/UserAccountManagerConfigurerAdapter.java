package ext.david.security.config;

import ext.david.security.AbstractUserAccountManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;

public abstract class UserAccountManagerConfigurerAdapter<M extends AbstractUserAccountManager> extends GlobalAuthenticationConfigurerAdapter {

    protected static final Logger LOGGER = LoggerFactory.getLogger(ext.david.security.config.UserAccountManagerConfigurer.class);

    private final UserAccountManagerConfigurer<M> configurer;
    private final M userAccountManager;

    public UserAccountManagerConfigurerAdapter(M userAccountManager) {
        LOGGER.trace("(CONIFG) CONSTRUCTOR: class={}, userAccountManager={}", toString(), userAccountManager);
        this.userAccountManager = userAccountManager;
        this.configurer = new UserAccountManagerConfigurer<>(userAccountManager);
    }

    public UserAccountManagerConfigurer<M> getConfigurer() {
        return configurer;
    }

    @Override
    public void init(AuthenticationManagerBuilder auth) throws Exception {
        LOGGER.trace("#init: auth={}", auth);
        LOGGER.debug("Applying {} for AutenticationManagerConfigurer: AccountManager={}", this.getClass().getName(), userAccountManager);
        auth.apply(configurer);
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        LOGGER.trace("#configure: auth={}", auth);
        configure(configurer);
    }

    public UserAccountManagerConfigurer<M>.UserAccountBuilderDelegate withUser(String username) {
        return configurer.withUser(username);
    }

    public void configure(UserAccountManagerConfigurer<M> config) {
    }

}
