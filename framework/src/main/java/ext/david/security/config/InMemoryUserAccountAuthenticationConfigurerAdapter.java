package ext.david.security.config;

import ext.david.model.core.account.UserAccount;
import ext.david.security.InMemoryUserAccountManager;

public class InMemoryUserAccountAuthenticationConfigurerAdapter extends UserAccountManagerConfigurerAdapter<InMemoryUserAccountManager> {

    private UserAccount godAccount;

    public InMemoryUserAccountAuthenticationConfigurerAdapter(InMemoryUserAccountManager userAccountManager) {
        super(userAccountManager);
    }

    @Override
    public void configure(UserAccountManagerConfigurer<InMemoryUserAccountManager> config) {
        LOGGER.trace("configure: config={}", config);
        if (godAccount != null) {
            LOGGER.info("Creating GOD account: {}", godAccount.getUsername());
            config.getUserDetailsService().createUser(godAccount);
        }
        super.configure(config);
    }

    public void setGodAccount(UserAccount godAccount) {
        this.godAccount = godAccount;
    }
}
