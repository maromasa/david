package ext.david.security.config;

import ext.david.security.DataJpaUserAccountManager;

public class DataJpaUserAccountAuthenticationConfigurerAdapter extends UserAccountManagerConfigurerAdapter<DataJpaUserAccountManager> {

    public DataJpaUserAccountAuthenticationConfigurerAdapter(DataJpaUserAccountManager dataJpaUserAccountManager) {
        super(dataJpaUserAccountManager);
    }

}
