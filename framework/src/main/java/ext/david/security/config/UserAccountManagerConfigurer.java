package ext.david.security.config;

import ext.david.model.core.account.UserAccount;
import ext.david.security.AbstractUserAccountManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.security.config.annotation.authentication.ProviderManagerBuilder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.userdetails.UserDetailsServiceConfigurer;
import org.springframework.security.core.userdetails.UserDetails;

import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;

/**
 * David UserBuilder support for instant creating when setup UserDetailsManager based AtuhenticationManagerConfigurer.
 */
public class UserAccountManagerConfigurer<M extends AbstractUserAccountManager> extends UserDetailsServiceConfigurer<AuthenticationManagerBuilder, UserAccountManagerConfigurer<M>, M> {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserAccountManagerConfigurer.class);

    private List<UserAccount.UserAccountBuilder> userAccountBuilders = new ArrayList<>();

    public UserAccountManagerConfigurer(M userAccountManager) {
        super(userAccountManager);
    }

    public UserAccountBuilderDelegate withUser(String username) {
        UserAccount.UserAccountBuilder builder = UserAccount.withUser(username);
        UserAccountBuilderDelegate delegate = new UserAccountBuilderDelegate(this, builder);
        userAccountBuilders.add(builder);
        return delegate;
    }

    @Override
    protected void initUserDetailsService() throws Exception {
        LOGGER.trace("#initUserDetailsService: targetUserDetailsService={}", getUserDetailsService());
        if (userAccountBuilders.size() > 0) {
            LOGGER.info("{} UserAccount builders are found.", userAccountBuilders.size());
            for (UserAccount.UserAccountBuilder builder : userAccountBuilders) {
                UserAccount userAccount = builder.build();
                getUserDetailsService().createUser((UserDetails) userAccount);
                LOGGER.info("Created: user={}", userAccount.getUsername());
            }
        }
    }

    public class UserAccountBuilderDelegate {

        private final UserAccountManagerConfigurer configurer;
        private final UserAccount.UserAccountBuilder builder;

        public UserAccountBuilderDelegate(UserAccountManagerConfigurer configurer, UserAccount.UserAccountBuilder builder) {
            this.configurer = configurer;
            this.builder = builder;
        }

        public UserAccountBuilderDelegate password(String password) {
            builder.password(password);
            return this;
        }

        public UserAccountBuilderDelegate username(String username) {
            builder.username(username);
            return this;
        }

        public UserAccountBuilderDelegate emailAddress(String emailAddress) {
            builder.emailAddress(emailAddress);
            return this;
        }

        public UserAccountBuilderDelegate roles(String... roles) {
            builder.roles(roles);
            return this;
        }

        public UserAccountManagerConfigurer and() {
            return configurer;
        }

    }

}
