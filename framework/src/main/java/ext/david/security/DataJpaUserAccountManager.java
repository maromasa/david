package ext.david.security;

import ext.david.model.core.account.UserAccount;
import ext.david.model.core.account.UserAccountImpl;
import ext.david.model.core.account.UserAccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
public class DataJpaUserAccountManager extends AbstractUserAccountManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataJpaUserAccountManager.class);
    private final UserAccountRepository userAccountRepository;

    public DataJpaUserAccountManager(UserAccountRepository userAccountRepository) {
        this.userAccountRepository = userAccountRepository;
    }

    @Override
    public void deleteUser(String username) {
        if (userExists(username)) {
            userAccountRepository.delete(username);
        }
    }

    @Override
    public void changePassword(String oldPassword, String newPassword) {
    }

    @Override
    public boolean userExists(String username) {
        return userAccountRepository.exists(username);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (!userExists(username)) {
            throw new UsernameNotFoundException(String.format("Username %s is not found in repository.", username));
        }
        UserDetails userDetails = userAccountRepository.findOne(username);
        LOGGER.debug("Getting UserAccount instance as UserDetails, from UserAccountJpaRepository: {}", userDetails);
        return userDetails;
    }

    @Override
    public void createUser(UserAccount userAccount) {
        saveOrUpdate(userAccount);
    }

    @Override
    public void updateUser(UserAccount userAccount) {
        saveOrUpdate(userAccount);
    }

    private void saveOrUpdate(UserAccount userAccount) {
        LOGGER.trace("#saveOrUpdate: userAccount={}", userAccount);
        if (userAccount instanceof UserAccountImpl) {
            userAccountRepository.save((UserAccountImpl) userAccount);
        } else {
            LOGGER.error("Could not create/update userAccount, because of NOT the instance of 'UserAccountImpl': {}", userAccount);
            //TODO:: implement if Argment UserAccount is not instance of UserAccountImpl
        }

    }
}
