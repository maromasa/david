package ext.david.security.oauth.web;

import ext.david.security.oauth.domain.ManagedApplication;
import ext.david.security.oauth.service.ManagedApplicationManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
@RequestMapping(path = {"/manage/applications", "/clients"})
public class ManagedApplicationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ManagedApplicationManager.class);
    private final ManagedApplicationManager applicationManager;

    public ManagedApplicationController(ManagedApplicationManager applicationManager) {
        this.applicationManager = applicationManager;
    }

    @RequestMapping(path = {"", "/", "/list"}, method = RequestMethod.GET)
    public String getList(Model model) {
        model.addAttribute(applicationManager.getAll());
        return "clientApplication/list";
    }

    @RequestMapping(path = {"/new"}, method = RequestMethod.GET)
    public String newGet(Model model) {
        model.addAttribute("mode", OPERATION_MODE.CREATE);
        model.addAttribute(new ManagedApplication());
        return "clientApplication/each";
    }

    @RequestMapping(path = {"/{id}"}, method = RequestMethod.GET)
    public String show(@PathVariable int id, Model model) {
        LOGGER.info("{}", id);
        model.addAttribute(applicationManager.getOne(id));
        return "clientApplication/show";

    }

    @RequestMapping(path = {"/new"}, method = RequestMethod.POST)
    public String newPost(@Valid  ManagedApplication managedApplication) {
        LOGGER.info("{}", managedApplication);
        return "clientApplication/new";
    }

    public enum OPERATION_MODE {
        CREATE,
        EDIT,
        VIEW
    }

}
