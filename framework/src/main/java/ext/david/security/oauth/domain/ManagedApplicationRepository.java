package ext.david.security.oauth.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ManagedApplicationRepository extends JpaRepository<ManagedApplication, Integer> {

    public ManagedApplication findByClientId(String clientId);

}
