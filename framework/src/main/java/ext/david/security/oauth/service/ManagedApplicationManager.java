package ext.david.security.oauth.service;

import ext.david.security.oauth.domain.ManagedApplication;
import ext.david.security.oauth.domain.ManagedApplicationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ManagedApplicationManager implements ClientDetailsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ManagedApplicationManager.class);
    private final ManagedApplicationRepository applicationRepository;

    public ManagedApplicationManager(ManagedApplicationRepository applicationRepository) {
        this.applicationRepository = applicationRepository;
    }

    @Override
    public ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {
        LOGGER.trace("#loadClientByClientId({})", clientId);
        if (applicationRepository.findByClientId(clientId) == null) {
            throw new ClientRegistrationException("Client is not registered: '" + clientId + "'");
        }
        return applicationRepository.findByClientId(clientId);
    }

    public List<ManagedApplication> getAll() {
        return applicationRepository.findAll();
    }

    public ManagedApplication resist(ManagedApplication managedApplication) {
        if (applicationRepository.findByClientId(managedApplication.getClientId()) != null) {
            LOGGER.warn("Application '{}' have already registered. Not saving.", managedApplication.getClientId());
        }
        applicationRepository.save(managedApplication);
        return managedApplication;
    }

    public ManagedApplication getOne(String clientId) {
        return applicationRepository.findByClientId(clientId);
    }

    public ManagedApplication getOne(int index) {
        return applicationRepository.findOne(index);
    }

    public boolean exists(int index) {
        return applicationRepository.exists(index);
    }
}
