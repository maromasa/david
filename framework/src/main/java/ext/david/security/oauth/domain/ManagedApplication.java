package ext.david.security.oauth.domain;

import com.sun.javafx.collections.UnmodifiableListSet;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.provider.ClientDetails;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.util.*;

@Entity
public class ManagedApplication implements ClientDetails {

    @Id
    @GeneratedValue
    private int id;

    @NotNull
    private String name;

    @NotNull
    private String serviceCode;
    private String description;
    private String clientId;
    private String clientSecret;

    @NotNull
    private String authorizationGrantType = "authorization_code";

    @NotNull
    private String redirectUrl;

    private String resourceIds;

    private String scope = "getAll";

    private Date registered;
    private boolean enabled;
    private int accessTokenValiditySeconds = 3600;
    private int refreshTokenValiditySeconds = 3600;

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getRegistered() {
        return registered;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String getClientId() {
        return this.clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @Override
    public Set<String> getResourceIds() {
        return null;
    }

    @Override
    public boolean isSecretRequired() {
        return true;
    }

    @Override
    public String getClientSecret() {
        return this.clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    @Override
    public boolean isScoped() {
        return false;
    }

    @Override
    public Set<String> getScope() {
        return new UnmodifiableListSet<String>(Arrays.asList(scope.split(" ")));
    }

    @Override
    public Set<String> getAuthorizedGrantTypes() {
        return new HashSet<String>(Arrays.asList(authorizationGrantType.split(" ")));
    }

    @Override
    public Set<String> getRegisteredRedirectUri() {
        return new UnmodifiableListSet<String>(Arrays.asList(redirectUrl));
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return Collections.EMPTY_SET;
    }

    @Override
    public Integer getAccessTokenValiditySeconds() {
        return this.accessTokenValiditySeconds;
    }

    @Override
    public Integer getRefreshTokenValiditySeconds() {
        return this.refreshTokenValiditySeconds;
    }

    @Override
    public boolean isAutoApprove(String scope) {
        return false;
    }

    @Override
    public Map<String, Object> getAdditionalInformation() {
        return null;
    }

    public static class Builder {

        public static ManagedApplication build(String serviceCode, String clientId, String clientSecret) {
            ManagedApplication application = new ManagedApplication();
            application.serviceCode = serviceCode;
            application.clientId = clientId;
            application.clientSecret = clientSecret;
            application.registered = new Date();
            return application;
        }

        public static ManagedApplication build(String serviceCode) {
            return build(serviceCode, UUID.randomUUID().toString(), UUID.randomUUID().toString());
        }
    }
}
