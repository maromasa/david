package ext.david.config.file;


import ext.david.file.AbstractFileProcessor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.util.concurrent.atomic.AtomicBoolean;

@RunWith(SpringRunner.class)
@ContextConfiguration
public class FileWatchServiceConfigurationTest {

    @Autowired
    private TestFileProcessor testFileProcessor;

    private AtomicBoolean called = new AtomicBoolean(false);

//    @Test
    public void test() {
        while (testFileProcessor.isCalled() == false) {

        }
    }

    @Configuration
    @Import(FileWatchServiceConfiguration.class)
    public static class TestConfiguration {

        @Bean
        public TestFileProcessor testFileProcessor() {
            return new TestFileProcessor();
        }

    }

    public static class TestFileProcessor extends AbstractFileProcessor {

        private AtomicBoolean called = new AtomicBoolean(false);

        public boolean isCalled() {
            return called.get();
        }

        @Override
        protected void processImpl(File file) {
            called.set(true);
        }
    }
}