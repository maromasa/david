package ext.david.file;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.util.List;

@RunWith(SpringRunner.class)
@ContextConfiguration
public class LegacyStatusFileFilterTest {

    @Autowired
    private LegacyStatusFileFilter legacyStatusFileFilter;

    private String testFileDirectoryName;

    @Before
    public void setup() {
        try {
            testFileDirectoryName = getClass().getClassLoader().getResource(getClass().getPackage().getName().replace(".", "/")).toURI().getPath();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void text_findDataFile() throws Exception {

        Method testMethod = legacyStatusFileFilter.getClass().getDeclaredMethod("findDataFile", File.class);
        testMethod.setAccessible(true);

        File testStatusFile = new File(String.format("%s/%s.STS", testFileDirectoryName, getClass().getSimpleName()));

        File result = null;

        result = (File) testMethod.invoke(legacyStatusFileFilter, testStatusFile);
        Assert.assertNotNull(result);

    }

    @Test
    public void test_filterFiles() {


        File testFileDirectory = new File(testFileDirectoryName);
        File[] testFiles = testFileDirectory.listFiles();

        List<File> result;

        result = legacyStatusFileFilter.filterFiles(testFiles);
        Assert.assertEquals(1, result.size());
    }

    @Configuration
    public static class TestConfiguration {

        @Bean
        public LegacyStatusFileFilter legacyStatusFileFilter() {
            LegacyStatusFileFilter legacyStatusFileFilter = new LegacyStatusFileFilter();
            legacyStatusFileFilter.setCaseSensitive(true);
            legacyStatusFileFilter.setExtension("STS");
            return legacyStatusFileFilter;
        }

    }

}