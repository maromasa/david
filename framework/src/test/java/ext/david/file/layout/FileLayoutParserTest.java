package ext.david.file.layout;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.springframework.core.io.ClassPathResource;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;

@RunWith(Parameterized.class)
public class FileLayoutParserTest {

    private static Method getLineParserMethod;
    private static Method determineLayoutRecordMethod;

    private FileLayoutParser.FormatType formatType;
    private String layoutId;
    private FileLayout fileLayout;
    private FileLayoutParser fileLayoutParser;

    public FileLayoutParserTest(FileLayoutParser.FormatType formatType, String layoutId) {

        this.formatType = formatType;
        this.layoutId = layoutId;

        fileLayout = FileLayoutManager.getLayout(layoutId);

        fileLayoutParser = new FileLayoutParser();
        fileLayoutParser.setFileLayout(fileLayout);
        fileLayoutParser.setParserCharset(Charset.forName(FileLayoutParser.DEFAULT_PARSER_CHARSET_NAME));
    }

    @BeforeClass
    public static void setup() {
        try {
            // TODO:: remove spring dependecy for obtain File object.
            FileLayoutManager.importJSON(new ClassPathResource("filelayout/FileLayoutLoaderTest.layout.json").getFile());

            getLineParserMethod = FileLayoutParser.class.getDeclaredMethod("getLineParser", FileLayoutParser.FormatType.class);
            determineLayoutRecordMethod = FileLayoutParser.class.getDeclaredMethod("determineLayoutRecord", FileLayoutParser.AbstractLineParser.class);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {FileLayoutParser.FormatType.FIXED_LENGTH, "single"},
                {FileLayoutParser.FormatType.FIXED_LENGTH, "multi"},
                {FileLayoutParser.FormatType.TSV, "single"},
                {FileLayoutParser.FormatType.TSV, "multi"},
        });
    }

    @Test
    public void test_getLineParser() {
        try {
            getLineParserMethod.setAccessible(true);
            Object lineParser = getLineParserMethod.invoke(fileLayoutParser, formatType);
            switch (formatType) {
                case FIXED_LENGTH:
                    Assert.assertTrue(lineParser instanceof FileLayoutParser.FixedLengthLineParser);
                    break;
                case CSV:
                case TSV:
                    Assert.assertTrue(lineParser instanceof FileLayoutParser.DelimitedLineParser);
                    break;
            }
            getLineParserMethod.setAccessible(false);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_determineLayoutRecord() {

        Optional.ofNullable(getInputTestFile()).ifPresent(file -> {

            getLineParserMethod.setAccessible(true);
            determineLayoutRecordMethod.setAccessible(true);

            try {
                FileLayoutParser.AbstractLineParser lineParser = (FileLayoutParser.AbstractLineParser) getLineParserMethod.invoke(fileLayoutParser, formatType);
                BufferedReader bufferedReader = Files.newBufferedReader(file.toPath(), StandardCharsets.UTF_8);
                String lineString;
                while ((lineString = bufferedReader.readLine()) != null) {
                    lineParser.setLineString(lineString);
                    LayoutRecord layoutRecord = (LayoutRecord) determineLayoutRecordMethod.invoke(fileLayoutParser, lineParser);
                    if (layoutId.equals("multi")) {
                        Assert.assertEquals(lineString.substring(0, 2), layoutRecord.getKeyValue());
                    } else {
                        Assert.assertEquals(fileLayout.getRecords().iterator().next(), layoutRecord);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }

        });

    }

    @Test
    public void test_parseLine() {

        Optional.ofNullable(getInputTestFile()).ifPresent(file -> {

            try (BufferedReader bufferedReader = Files.newBufferedReader(file.toPath(), StandardCharsets.UTF_8)) {

                String lineString;
                while ((lineString = bufferedReader.readLine()) != null) {
                    Map<LayoutField, Object> parsedMaps = fileLayoutParser.parseLine(lineString, formatType);
                    if (layoutId.equals("multi")) {
                        if (lineString.startsWith("01")) {
                            Assert.assertEquals(4, parsedMaps.size());
                        } else {
                            Assert.assertEquals(3, parsedMaps.size());
                        }

                    } else {
                        Assert.assertEquals(4, parsedMaps.size());
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });


    }

    private File getInputTestFile() {
        try {
            String name = String.format("%s.%s.%s.txt", getClass().getName().replace(".", "/"), formatType.name(), layoutId);
            URL url = getClass().getClassLoader().getResource(name);
            if (url != null) {
                return new File(url.toURI());
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

}