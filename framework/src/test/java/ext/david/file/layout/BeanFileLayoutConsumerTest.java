package ext.david.file.layout;

import java.util.Date;

public class BeanFileLayoutConsumerTest {

    private BeanParserConsumer<TestBean> consumer;

    public static class TestBean {

        private String partner;
        private String partnerName;
        private Date date;
        private long value;

        public String getPartnerName() {
            return partnerName;
        }

        public void setPartnerName(String partnerName) {
            this.partnerName = partnerName;
        }

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }

        public long getValue() {
            return value;
        }

        public void setValue(long value) {
            this.value = value;
        }

        public String getPartner() {
            return partner;
        }

        public void setPartner(String partner) {
            this.partner = partner;
        }
    }

}