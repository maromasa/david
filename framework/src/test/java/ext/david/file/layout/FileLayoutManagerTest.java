package ext.david.file.layout;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FileLayoutManagerTest {

    @Test
    public void testImportFromFile() {
        try {
            FileLayoutManager.importJSON(new ClassPathResource("ext/david/file/layout/FileLayoutManager.layout.json").getFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Configuration
    public static class TextConfiguration {
    }


}