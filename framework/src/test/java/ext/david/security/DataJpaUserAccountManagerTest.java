package ext.david.security;

import ext.david.model.core.account.UserAccount;
import ext.david.model.core.account.UserAccountRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest(showSql = true)
@TestPropertySource(properties = {"spring.datasource.data=classpath:ext/davide/test/users.sql"})
public class DataJpaUserAccountManagerTest {

    @Autowired
    private DataJpaUserAccountManager userAccountManager;

    @Test
    public void test_loadUserByName() {
        UserDetails userDetails = userAccountManager.loadUserByUsername("gchastney0");
        Assert.assertNotNull(userDetails);
        Assert.assertTrue(userDetails instanceof UserAccount);
    }

    @Test
    public void test_userExists() {
        Assert.assertFalse(userAccountManager.userExists("poGGi2LvbvE"));
    }

    @Test
    public void test_getUser() {
        UserDetails userDetails = userAccountManager.loadUserByUsername("cbienvenu4");
        Assert.assertTrue(userDetails instanceof UserAccount);
        Assert.assertEquals("cvanderveldt4@foxnews.com", ((UserAccount) userDetails).getEmailAddress());
    }

    @Test
    public void test_crateUser() {
        UserAccount userAccount = UserAccount.withUser("thaime5").password("cK5Gi1Z8").emailAddress("asnaddin2@a8.net").build();
        userAccountManager.createUser(userAccount);
    }

    @Configuration
    @EntityScan(basePackageClasses = {UserAccount.class})
    @EnableJpaRepositories(basePackageClasses = {UserAccountRepository.class})
    public static class ContextConfiguration {

        @Bean
        public DataJpaUserAccountManager userManager(UserAccountRepository userAccountRepository) {
            return new DataJpaUserAccountManager(userAccountRepository);
        }

    }

}