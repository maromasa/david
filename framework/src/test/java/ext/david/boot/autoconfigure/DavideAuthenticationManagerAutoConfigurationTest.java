package ext.david.boot.autoconfigure;

import ext.david.boot.autoconfigure.annotation.EnableJpaUserAccountManager;
import ext.david.boot.autoconfigure.security.DavidAuthenticationAutoConfiguration;
import ext.david.security.domain.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DavideAuthenticationManagerAutoConfigurationTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProviderManager providerManager;

    @Test
    public void test() {
        for(AuthenticationProvider provider : providerManager.getProviders()) {
            System.out.println(provider);
        }
    }

    @Configuration
    @EnableJpaUserAccountManager
    @EnableAutoConfiguration
    @ImportAutoConfiguration(DavidAuthenticationAutoConfiguration.class)
    public static class ContextConfiguration {
    }
}
