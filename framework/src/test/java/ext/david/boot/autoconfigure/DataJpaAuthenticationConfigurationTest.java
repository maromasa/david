package ext.david.boot.autoconfigure;

import ext.david.boot.autoconfigure.security.DataJpaAuthenticationConfiguration;
import ext.david.boot.autoconfigure.security.EnableJpaUserAccountManagerConfiguration;
import ext.david.security.DataJpaUserAccountManager;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
@ImportAutoConfiguration(value = {EnableJpaUserAccountManagerConfiguration.class, DataJpaAuthenticationConfiguration.class})
public class DataJpaAuthenticationConfigurationTest {

    @Autowired(required = false)
    private DataJpaUserAccountManager userManager;

    @Test
    public void test() {
        Assert.assertNotNull(userManager);
    }

    @Configuration
    public static class Context {

    }

}