package ext.david.boot.autoconfigure;

import ext.david.boot.autoconfigure.security.DavidSecurityProperties;
import ext.david.boot.autoconfigure.security.InMemoryAuthenticationConfiguration;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ImportAutoConfiguration(InMemoryAuthenticationConfiguration.class)
public class InMemoryAuthenticationConfigurationTest {

    @Autowired
    private ProviderManager authenticationManager;

    @Autowired
    private DavidSecurityProperties davidSecurityProperties;

    @Test
    public void test() {
        Authentication authentication = new UsernamePasswordAuthenticationToken(davidSecurityProperties.getGod().getName(), davidSecurityProperties.getGod().getPassword());
        Authentication result = authenticationManager.authenticate(authentication);
        Assert.assertTrue(result.isAuthenticated());
    }

    @Configuration
    @EnableAutoConfiguration
    public static class ContextConfiguration {

        @Bean
        public DavidSecurityProperties davideSecurityProperties() {
            return new DavidSecurityProperties();
        }

    }

}