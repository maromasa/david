package ext.david.boot.autoconfigure.file;

import ext.david.file.layout.FileLayoutManager;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class FileLayoutLoaderTest {

    @Test
    public void test() {
        Assert.assertEquals(1, FileLayoutManager.getAll().size());
    }

    @Configuration
    public static class TestConfiguration {

        @Bean
        public FileLayoutLoader fileLayoutLoader() {
            FileLayoutLoader fileLayoutLoader = new FileLayoutLoader();
            fileLayoutLoader.setPrefix(String.format("classpath:%s/", getClass().getPackage().getName().replace(".", "/")));
            return fileLayoutLoader;
        }
    }


}