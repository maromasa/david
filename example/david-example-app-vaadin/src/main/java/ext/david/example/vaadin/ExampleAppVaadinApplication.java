package ext.david.example.vaadin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExampleAppVaadinApplication {


    public static void main(String[] args) {
        SpringApplication.run(ExampleAppVaadinApplication.class, args);
    }

}
