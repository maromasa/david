package ext.david.example.vaadin.view;

import com.vaadin.data.Binder;
import com.vaadin.data.converter.LocalDateToDateConverter;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.TextField;
import ext.david.example.vaadin.model.SalesOrder;
import ext.david.ui.vaadin.AbstractCrudView;
import ext.david.ui.vaadin.SideBarContent;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@SideBarContent(caption = "Confirm Sales Order")
public class SalesOrderView extends AbstractCrudView<SalesOrder> {

    private List<SalesOrder> salesOrderList = new ArrayList<>();

    public SalesOrderView() {
        super("Sales Orders");
    }

    @Override
    protected AbstractEntityDetail createEntityDetail(FormMode formMode) {
        return new SalesOrderDetail(formMode);
    }

    @Override
    protected void configureGrid(Grid<SalesOrder> grid) {

    }

    @Override
    protected Class<SalesOrder> getEntityClass() {
        return SalesOrder.class;
    }

    @Override
    protected Collection<SalesOrder> getInitialData() {
        return this.salesOrderList;
    }

    @Override
    protected SalesOrder getNewData() {
        return new SalesOrder(Long.toString(System.currentTimeMillis()));
    }

    @Override
    protected Class<? extends AbstractEntityDetail> getEntityDetailClass() {
        return SalesOrderDetail.class;
    }

    public class SalesOrderDetail extends AbstractEntityDetail {

        private final LocalDateToDateConverter localDateToDateConverter;
        TextField idField = new TextField("id");
        TextField customerOrderNumberField = new TextField("Customer Order Number");
        DateField orderDateField = new DateField("Order Date");
        TextField partnerField = new TextField("Partner");

        public SalesOrderDetail(FormMode formMode) {
            super(formMode);
            localDateToDateConverter = new LocalDateToDateConverter(OffsetDateTime.now().getOffset());
        }

        @Override
        protected void initForCreate(ComponentContainer formControls, Binder<SalesOrder> binder) {
            formControls.addComponents(idField, partnerField, customerOrderNumberField, orderDateField);
            idField.setReadOnly(true);
            binder.forField(idField).bind("id");
            binder.forField(customerOrderNumberField).asRequired("").bind("customerOrderNumber");
            binder.forField(partnerField).asRequired("").bind("partner");
            binder.forField(orderDateField).withConverter(localDateToDateConverter).bind("salesOrderDate");
        }

        @Override
        protected void initForModify(ComponentContainer formControls, Binder<SalesOrder> binder) {

        }

        @Override
        protected void initForPreview(ComponentContainer formControls, Binder<SalesOrder> binder) {

        }

        @Override
        protected void initForDetail(ComponentContainer formControls, Binder<SalesOrder> binder) {

        }
    }
}
