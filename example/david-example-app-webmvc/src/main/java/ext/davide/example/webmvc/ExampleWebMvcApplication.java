package ext.davide.example.webmvc;

import ext.david.config.DavidWebApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(DavidWebApplication.class)
public class ExampleWebMvcApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExampleWebMvcApplication.class, args);
    }

}
