INSERT INTO APPLICATION_USER (
    USERNAME
    , EMAIL
    , ENABLED
    , LAST_LOGIN
    , PASSWORD
    , REGISTERED
) VALUES (
    'admin'
    , 'admin@admin.com'
    , true
    , timestamp '2017-01-01 00:00:00'
    , 'admin'
    , timestamp '2017-01-01 00:00:00'
);

INSERT INTO APPLICATION_USER_ROLES (
    APPLICATION_USER_USERNAME
    , ROLES
) VALUES (
    'admin'
    , 'ROLE_ADMIN'
), (
    'admin'
    , 'ROLE_USER'
);

INSERT INTO MANAGED_APPLICATION
(
    ID
  , NAME
  , SERVICE_CODE
  , DESCRIPTION
  , CLIENT_ID
  , CLIENT_SECRET
  , AUTHORIZATION_GRANT_TYPE
  , REDIRECT_URL
  , ACCESS_TOKEN_VALIDITY_SECONDS
  , REFRESH_TOKEN_VALIDITY_SECONDS
  , RESOURCE_IDS
  , SCOPE
  , REGISTERED
  , ENABLED
)
VALUES
(
    1
  , 'sample'
  , '0000S0000'
  , 'a sample client'
  , 'sample'
  , 'sample_secret'
  , 'authorization_code'
  , 'http://localost:8088s'
  , 3600
  , 3600
  , null
  , 'all'
  , TIMESTAMP '2017-01-01 00:00:00'
  , true
), (
    2
  , 'daim'
  , '0000S0001'
  , 'daim sample client'
  , 'daim'
  , 'daim_secret'
  , 'authorization_code'
  , 'http://localhost:8888/daim'
  , 3600
  , 3600
  , null
  , 'all'
  , TIMESTAMP '2017-01-01 00:00:00'
  , true
);
