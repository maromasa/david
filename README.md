# David

*Instant Java Application framework for internal business systems.*

David (/ˈdeɪvɪd/)

## Features

- Integrated Authentication and Authorization for company internal.
- Starter POMs and some libraries are ready for quick ui building.
- Build-in file importing (polling and fixed file parser)
- Build-in workflow functions.
- Build-n Reporting functions.

## Getting Started
